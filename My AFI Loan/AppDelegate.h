//
//  AppDelegate.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworkReachabilityManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@end

