//
//  JSONModel+AKExtension.h
//  ETB
//
//  Created by Ashik Ahmad on 12/2/14.
//  Copyright (c) 2014 AAPBD. All rights reserved.
//

#import "JSONModel.h"

#define NSStringFromProperty(property) NSStringFromSelector(@selector(property))

@interface JSONModel (AKExtension)

@end
