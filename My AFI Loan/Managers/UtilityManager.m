//
//  InputValidator.m
//
//  Created by Dulal Hossain on 22/11/2013.
//  Copyright (c) 2013 Aviel Gross. All rights reserved.
//

#import "UtilityManager.h"

@implementation UtilityManager
{
}
+(NSString*)findOutValue:(NSString*)key source:(NSDictionary*)dict{
    NSString *temp=@"";
    int ind=-1;
    for (int index=0; index<[[dict allKeys] count]; index++) {
        if ([key isEqualToString:[[dict allKeys] objectAtIndex:index]]) {
            ind=index;
            temp=[dict valueForKey:[[dict allKeys] objectAtIndex:index]];
            break;
        }
    }
    return temp;
}
+(BOOL) validateEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
