//
//  ETBHTTPRequestManager.m
//  ETB
//
//  Created by Ashik Ahmad on 12/1/14.
//  Copyright (c) 2014 AAPBD. All rights reserved.
//

#import "AFILoanRequestManager.h"

@implementation AFILoanRequestManager

+(AFILoanRequestManager *) manager {
    static dispatch_once_t pred;
    static AFILoanRequestManager *shared = nil;
    dispatch_once(&pred, ^{
         NSURL *baseURL = [NSURL URLWithString:@"http://test.myafiloan.com/api/"];
      
        shared = [[AFILoanRequestManager alloc] initWithBaseURL:baseURL];
        
        AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer new];
     
        serializer.readingOptions=NSJSONReadingAllowFragments;
        serializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
        shared.responseSerializer = serializer;
        
        shared.requestSerializer = [AFJSONRequestSerializer serializer];
     
    });
    return shared;
}

@end
