//
//  AppUserDefault.h
//  PartyLink
//
//  Created by AAPBD Mac mini on 17/11/2014.
//  Copyright (c) 2014 RB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUserDefault : NSObject
+ (AppUserDefault*) sharedInstance;
-(void)setUserDefaultBoolValue:(BOOL)value key:(NSString*)key;
-(BOOL)getUserDefaultBoolValue:(NSString*)key;
-(void)setUserDefaultStringValue:(NSString*)value key:(NSString*)key;
-(NSString*)getStringValueforKey:(NSString*)key;
-(NSInteger)getIntegerValueforKey:(NSString*)key;
-(void)setUserDefaultIntegerValue:(NSInteger)value key:(NSString*)key;
-(void)removeUserDefaultValue:(NSString*)key;
@end
