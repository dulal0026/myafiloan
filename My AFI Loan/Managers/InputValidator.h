//
//  InputValidator.h
//  Seslenen Kitap
//
//  Created by Dulal Hossain on 22/11/2013.
//  Copyright (c) 2013 Aviel Gross. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface InputValidator : NSObject
+(BOOL) validateEmail:(NSString *)checkString;
+(NSString *)validateCreditCard:(NSString *)enteredCard;
// ---- Sujohn ----
+ (BOOL)isPasswordFormValid:(NSString *)oldPass newpass:(NSString *)newpass rePass:(NSString*)rePass;

// Broker
+(BOOL)isBrokerInfoValid:(NSString *)brokerage address:(NSString *)address phone:(NSString *)phone;

// Agent
+ (BOOL)isAgentInfoValid:(NSString *)firstName lastName:(NSString *)lastName licenceNum:(NSString*)licenceNum brokerNum:(NSString*)brokerNum address:(NSString*)address officePhone:(NSString*)officePhone mobilePhone:(NSString*)mobilePhone additionalRecipient:(NSString*)additionalRecipient;

+ (BOOL)isAgentFooterInfoValid:(NSString *)company website:(NSString *)website facebook:(NSString*)facebook twitter:(NSString*)twitter linkedin:(NSString*)linkedin;

//New Loan create Validation
+ (BOOL)validateCreateLoanForm:(NSString *)firstName lastName:(NSString *)lastName email:(NSString*)email;

+ (BOOL)isLoanOfficerInfoValid:(NSString *)website facebook:(NSString *)facebook twitter:(NSString*)twitter linkedIn:(NSString*)linkedIn;

+ (BOOL)isEmailValid:(NSString *)email;

+ (BOOL)isLoanInfoValidBBA:(NSString *)purchasePrice otherFinacing:(NSString *)otherFinacing hazardInsurance:(NSString*)hazardInsurance realStateTaxes:(NSString*)realStateTaxes HOAFees:(NSString*)HOAFees otherExpenses:(NSString*)otherExpenses;
@end
