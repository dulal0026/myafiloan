//
//  InputValidator.h
//  Seslenen Kitap
//
//  Created by Dulal Hossain on 22/11/2013.
//  Copyright (c) 2013 Aviel Gross. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UtilityManager : NSObject
+(NSString*)findOutValue:(NSString*)key source:(NSDictionary*)dict;
+(BOOL) validateEmail:(NSString *)checkString;
@end
