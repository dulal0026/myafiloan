//
//  ApplicationManager
//
//  Created by Dulal Hossain on 22/11/2013.
//  Copyright (c) 2013 Aviel Gross. All rights reserved.
//

#import <Foundation/Foundation.h>

#define emailValidationMsg @"Invalid email address"
#define emailEmptyMsg @"Please enter an email address"
#define noInternetAlertMsg @"No Internet Connection availalbe."

#define sessionKey @"sessionKey"
#define userTypeKey @"UserTypeKey"
#define lastUpdatedDate @"LastUpdatedDateKey"
@interface ApplicationTextManager : NSObject{
}

@end
