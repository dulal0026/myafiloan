//
//  InputValidator.m
//
//  Created by Dulal Hossain on 22/11/2013.
//  Copyright (c) 2013 Aviel Gross. All rights reserved.
//

#import "InputValidator.h"
#import "AlertManager.h"

@implementation InputValidator
{
}

+(BOOL) validateEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
+(NSString *)validateCreditCard:(NSString *)enteredCard
{
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$" options:NSRegularExpressionCaseInsensitive error:&error];
    NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:enteredCard options:0 range:NSMakeRange(0, [enteredCard length])];
    if (!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0)))
    {
        NSString *substringForFirstMatch = [enteredCard substringWithRange:rangeOfFirstMatch];
        //NSLog(@"Extracted URL: %@",substringForFirstMatch);
        return substringForFirstMatch;
    }
    return NULL;
}
// --- Sujohn ----
+ (BOOL)isPasswordFormValid:(NSString *)oldPass newpass:(NSString *)newpass rePass:(NSString*)rePass
{
    NSString *errorMessage=@"";
    
    if ([oldPass isEqualToString:@""]){
        errorMessage = @"Please enter old password.";
    }
    else if ([newpass isEqualToString:@""]){
        errorMessage = @"Please enter new password.";
    }
    else if ([rePass isEqualToString:@""]){
        errorMessage = @"Please repeat new password.";
    }
    else if (newpass.length > 0 && rePass.length > 0){
        if (![newpass isEqualToString:rePass]) {
            errorMessage = @"Password didn't match.";
        }
    }
    
    if (errorMessage.length>0) {
        [AlertManager showAlertSingle:@"Validation Error!!" msg:errorMessage];
        return NO;
    }
    else
        return YES;
}

// Broker
+(BOOL)isBrokerInfoValid:(NSString *)brokerage address:(NSString *)address phone:(NSString *)phone
{
    NSString *errorMessage;
    
    if ([brokerage isEqualToString:@""]){
        errorMessage = @"Please enter brokerage.";
    }
    else if ([address isEqualToString:@""]){
        errorMessage = @"Please enter address.";
    }
    else if ([phone isEqualToString:@""]){
        errorMessage = @"Please enter phone number.";
    }
    
    if (errorMessage) {
        [AlertManager showAlertSingle:@"Validation Error!!" msg:errorMessage];
        return NO;
    }
    else
        return YES;
}

// Agent
+ (BOOL)isAgentInfoValid:(NSString *)firstName lastName:(NSString *)lastName licenceNum:(NSString*)licenceNum brokerNum:(NSString*)brokerNum address:(NSString*)address officePhone:(NSString*)officePhone mobilePhone:(NSString*)mobilePhone additionalRecipient:(NSString*)additionalRecipient
{
    NSString *errorMessage = nil;
    
    if ([firstName isEqualToString:@""])
        errorMessage = @"Please enter firstName.";
    
    else if ([lastName isEqualToString:@""])
        errorMessage = @"Please enter lastName.";
    
    else if ([licenceNum isEqualToString:@""])
        errorMessage = @"Please enter licenceNum.";
    
    else if ([brokerNum isEqualToString:@""])
        errorMessage = @"Please enter brokerNum.";
    
    else if ([address isEqualToString:@""])
        errorMessage = @"Please enter address.";
    
    else if ([officePhone isEqualToString:@""])
        errorMessage = @"Please enter officePhone.";
    
    else if ([mobilePhone isEqualToString:@""])
        errorMessage = @"Please enter mobilePhone.";
    
    else if ([additionalRecipient isEqualToString:@""])
        errorMessage = @"Please enter additionalRecipient.";
    
    if (errorMessage) {
        [AlertManager showAlertSingle:@"Validation Error!!" msg:errorMessage];
        return NO;
    }
    else
        return YES;
}
+ (BOOL)isAgentFooterInfoValid:(NSString *)company website:(NSString *)website facebook:(NSString*)facebook twitter:(NSString*)twitter linkedin:(NSString*)linkedin{
    NSString *errorMessage = nil;
    
    if ([company isEqualToString:@""])
        errorMessage = @"Please enter company Name.";
    
    else if ([website isEqualToString:@""])
        errorMessage = @"Please enter website address.";
    
    else if ([facebook isEqualToString:@""])
        errorMessage = @"Please enter facebook address.";
    
    else if ([twitter isEqualToString:@""])
        errorMessage = @"Please enter twitter address.";
    
    else if ([linkedin isEqualToString:@""])
        errorMessage = @"Please enter linkedIn address.";
    
    if (errorMessage) {
        [AlertManager showAlertSingle:@"Validation Error!!" msg:errorMessage];
        return NO;
    }
    else
        return YES;
}
+ (BOOL)isLoanInfoValidBBA:(NSString *)purchasePrice otherFinacing:(NSString *)otherFinacing hazardInsurance:(NSString*)hazardInsurance realStateTaxes:(NSString*)realStateTaxes HOAFees:(NSString*)HOAFees otherExpenses:(NSString*)otherExpenses{
    NSString *errorMessage = nil;
    
    if ([purchasePrice isEqualToString:@""])
        errorMessage = @"Please enter purchase price.";
    
    else if ([otherFinacing isEqualToString:@""])
        errorMessage = @"Please enter other financing P&I.";
    
    else if ([hazardInsurance isEqualToString:@""])
        errorMessage = @"Please enter hazard insurance.";
    
    else if ([realStateTaxes isEqualToString:@""])
        errorMessage = @"Please enter real estate taxes.";
    
    else if ([HOAFees isEqualToString:@""])
        errorMessage = @"Please enter HOA Dues.";
    
    else if ([otherExpenses isEqualToString:@""])
        errorMessage = @"Please enter other expenses.";
    
    if (errorMessage) {
        [AlertManager showAlertSingle:@"Validation Error!!" msg:errorMessage];
        return NO;
    }
    else
        return YES;
    
}
//New Loan create Validation
+ (BOOL)validateCreateLoanForm:(NSString *)firstName lastName:(NSString *)lastName email:(NSString*)email{
    NSString *errorMessage = nil;
    
    if ([firstName isEqualToString:@""])
        errorMessage = @"Please enter firstName.";
    
    else if ([lastName isEqualToString:@""])
        errorMessage = @"Please enter lastName.";
    
    else if ([email isEqualToString:@""])
        errorMessage = @"Please enter licenceNum.";

    else if (![self validateEmail:email]) errorMessage=@"Email Address is not Valid";

    if (errorMessage) {
        [AlertManager showAlertSingle:@"Validation Error!!" msg:errorMessage];
        return NO;
    }
    else
        return YES;
}
+ (BOOL)isLoanOfficerInfoValid:(NSString *)website facebook:(NSString *)facebook twitter:(NSString*)twitter linkedIn:(NSString*)linkedIn{
    NSString *errorMessage = nil;
    
    if ([website isEqualToString:@""])
        errorMessage = @"Please enter website.";
    
    else if ([facebook isEqualToString:@""])
        errorMessage = @"Please enter facebook link.";
    
    else if ([twitter isEqualToString:@""])
        errorMessage = @"Please enter Twitter.";
    
    else if ([linkedIn isEqualToString:@""])
        errorMessage = @"Please enter LinkedIn.";
    
    if (errorMessage) {
        [AlertManager showAlertSingle:@"Validation Error!!" msg:errorMessage];
        return NO;
    }
    else
        return YES;
}
+ (BOOL)isEmailValid:(NSString *)email{
    NSString *responseMessage=nil;
    if ([email isEqualToString:@""])
    {
        responseMessage=@"Please enter an email address.";
    }
    else if (![self validateEmail:email])
    {
        responseMessage=@"Invalid email address.";
    }
    if (responseMessage) {
        [AlertManager showAlertSingle:@"Validation Error!!" msg:responseMessage];
        return NO;
    }
    else
        return YES;
}

@end
