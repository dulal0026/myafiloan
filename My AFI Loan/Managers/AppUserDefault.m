//
//  AppUserDefault.m
//  PartyLink
//
//  Created by AAPBD Mac mini on 17/11/2014.
//  Copyright (c) 2014 RB. All rights reserved.
//

#import "AppUserDefault.h"
static AppUserDefault *sharedDataInstance = nil;
@implementation AppUserDefault
-(void)setUserDefaultBoolValue:(BOOL)value key:(NSString*)key{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setBool:value forKey:key];
    [defaults synchronize];
}
-(void)removeUserDefaultValue:(NSString*)key{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}
-(BOOL)getUserDefaultBoolValue:(NSString*)key{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    BOOL boolvalue=[defaults boolForKey:key];
    return boolvalue;
}
-(void)setUserDefaultStringValue:(NSString*)value key:(NSString*)key{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}
-(NSString*)getStringValueforKey:(NSString*)key{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *strValue =[defaults objectForKey:key];
    return strValue;
}
-(NSInteger)getIntegerValueforKey:(NSString*)key{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSInteger intValue =[defaults integerForKey:key];
    return intValue;
}
-(void)setUserDefaultIntegerValue:(NSInteger)value key:(NSString*)key{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:key];
    [defaults synchronize];
}
+ (AppUserDefault*) sharedInstance
{
    @synchronized(self){
        if (sharedDataInstance == nil) sharedDataInstance =  [[self alloc] init];
    }
    return sharedDataInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedDataInstance == nil){
            sharedDataInstance = [super allocWithZone:zone];
            return sharedDataInstance;
        }
    }
    return nil;
}
// setup the data collection
-init
{
    if (self = [super init])
    {
    }
    return self;
}
@end

