//
//  UITextField+CustomBorder.h
//  Seslenen Kitap
//
//  Created by Dulal Hossain on 05/03/2014.
//  Copyright (c) 2014 Magnifo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UITextField (CustomBorder)
-(void)setBorderColor;
-(void)setPadding;

@end
