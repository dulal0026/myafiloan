//
//  UITextField+CustomBorder.m
//  Seslenen Kitap
//
//  Created by Dulal Hossain on 05/03/2014.
//  Copyright (c) 2014 Magnifo. All rights reserved.
//

#import "UITextField+CustomBorder.h"

@implementation UITextField (CustomBorder)

-(void)setPadding{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, self.frame.size.height)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}
-(void)setBorderColor{
    self.borderStyle=UITextBorderStyleNone;
    self.layer.masksToBounds=YES;
    self.layer.borderWidth= 1.0f;
    self.layer.borderColor=[[UIColor colorWithRed:150.0/255 green:150.0/255 blue:150.0/255 alpha:1.0]CGColor];
}

@end
