//
//  MHSSelectHeader.h
//  Mahas
//
//  Created by Ashik Uddin Ahmad on 2/22/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountHeader : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *collapseButton;
@property (weak, nonatomic) IBOutlet UIView *backView;

@property (assign, nonatomic) NSInteger section;

@end
