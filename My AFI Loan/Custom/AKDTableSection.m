//
//  AKDTableSection.m
//  Mahas
//
//  Created by Ashik Uddin Ahmad on 2/23/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "AKDTableSection.h"

@implementation AKDTableSection

-(NSMutableArray *)rowsData {
    if (!_rowsData) {
        _rowsData = [NSMutableArray array];
    }
    return _rowsData;
}

@end
