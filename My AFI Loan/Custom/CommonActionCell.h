//
//  CommonActionCell.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/30/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonActionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@end
