//
//  CommonInputSwitchCell.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonInputSwitchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *syncSwitch;
@end
