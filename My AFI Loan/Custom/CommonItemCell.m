//
//  CommonItemCell.m
//  ETB
//
//  Created by Ashik Ahmad on 12/1/14.
//  Copyright (c) 2014 AAPBD. All rights reserved.
//

#import "CommonItemCell.h"

@interface CommonItemCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *featureImageWidthConstraint;

@end

@implementation CommonItemCell {
    BOOL _featured;
    CGFloat defaultfeatureImageWidth;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    defaultfeatureImageWidth = self.featureImageWidthConstraint.constant;
    self.featured = NO;
}

-(void)setFeatured:(BOOL)featured {
    _featured = featured;
    if (featured) {
        self.featureImageWidthConstraint.constant = defaultfeatureImageWidth;
    } else {
        self.featureImageWidthConstraint.constant = 0;
    }
}

-(BOOL)featured {
    return _featured;
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

@end
