//
//  AKDTableSection.h
//  Mahas
//
//  Created by Ashik Uddin Ahmad on 2/23/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AKDTableSection : NSObject

@property (strong, nonatomic) id sectionInfo;
@property (assign, nonatomic) NSInteger typeID;
@property (assign, nonatomic) CGFloat sectionHeaderHeight;
@property (assign, nonatomic) BOOL isCollapsed;
@property (strong, nonatomic) NSString *inputValue;
@property (assign, nonatomic) NSInteger rowType;
@property (strong, nonatomic) NSString *pictureUrl;


@property (strong, nonatomic) NSMutableArray *rowsData;

@end
