//
//  MHSSelectHeader.m
//  Mahas
//
//  Created by Ashik Uddin Ahmad on 2/22/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "OptionalFieldHeader.h"

@implementation OptionalFieldHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib {
    [super awakeFromNib];
    self.addButton.hidden=YES;
   
    self.deviderImageView.image = [[UIImage imageNamed:@"stich_line"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.deviderImageView setTintColor:[UIColor redColor]];
    self.inputTextField.hidden=YES;
}

@end
