//
//  DLTableRow.h
//  MYAFILOAN
//
//  Created by Dulal Hossain on 05/18/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DLTableRow : NSObject

@property (assign, nonatomic) NSInteger typeID;
@property (strong, nonatomic) id rowInfo;
@property (assign, nonatomic) CGFloat rowHeight;
@property (strong, nonatomic) NSString *rowTitle;
@property (strong, nonatomic) NSString *rowValue;
@property (strong, nonatomic) NSString *email;
@property (assign, nonatomic) BOOL boolValue;

@end
