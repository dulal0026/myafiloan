//
//  PrequalTriOptionCell.m
//  ETB
//
//  Created by Ashik Ahmad on 12/1/14.
//  Copyright (c) 2014 AAPBD. All rights reserved.
//

#import "PrequalTriOptionCell.h"

@interface PrequalTriOptionCell ()


@end

@implementation PrequalTriOptionCell {
 
}
-(void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
    self.bgView.layer.shadowOffset = CGSizeMake(5, 0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 1.0;

}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}
@end
