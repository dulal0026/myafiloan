//
//  PrequalTriOptionCell.h
//  ETB
//
//  Created by Ashik Ahmad on 12/1/14.
//  Copyright (c) 2014 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface PrequalTriOptionCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UIButton *disclossureButton;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
