//
//  MHSSelectHeader.h
//  Mahas
//
//  Created by Ashik Uddin Ahmad on 2/22/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionalFieldHeader : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *collapseButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *deviderImageView;
@property (weak, nonatomic) IBOutlet UITextField *inputTextField;

@property (assign, nonatomic) NSInteger section;

@end
