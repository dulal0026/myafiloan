//
//  AgentDetailsVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "AgentInfo.h"
#import "AgentDetailsCell.h"
#import "ReinviteCell.h"
#import "UIImageView+AFNetworking.h"
#import "AFILoanRequestManager.h"
#import "SVProgressHUD.h"
#import "AlertManager.h"
#import "ApplicationTextManager.h"
#import "AppUserDefault.h"

@interface AgentDetailsVC : UIViewController
@property(nonatomic,strong)AgentInfo *agentInfo;
-(IBAction)goBack:(id)sender;
-(IBAction)showMenu:(id)sender;

@property(nonatomic,strong)NSMutableArray *agentArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *agentImageView;
@property (weak, nonatomic) IBOutlet UILabel *agentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *agentStatusLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeaderHeightConstraint;
@end
