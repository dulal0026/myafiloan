//
//  LoanDetailsVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/16/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#define SYSTEM_VERSION                              ([[UIDevice currentDevice] systemVersion])
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IS_IOS8_OR_ABOVE                            (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))

#import "LoanDetailsBBAVC.h"
#import "AccountHeader.h"
#import "AFNetworkReachabilityManager.h"
#import "AlertManager.h"
#import "SVProgressHUD.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"

typedef NS_ENUM(NSInteger, HeaderType) {
    emptyHeader,
    monthlyFeesHeader,
    loanContactHeader
};
typedef NS_ENUM(NSInteger, SectionRowType) {
    commonInputRow,
    monthlyFeesRow,
    commonActionRow,
    loanContactRow
};

@interface LoanDetailsBBAVC ()
{
    AgentModificationStatus *agentModificationStatus;
}
@property (nonatomic, strong) LoanContactCell *loanContactCell;
@property (nonatomic, strong) CommonActionCell *commonActionCell;
@property (nonatomic, strong) CommonInputCell *commonInputCell;
@property (nonatomic, strong) MonthlyFeesInputCell *monthlyFeesInputCell;

@end

@implementation LoanDetailsBBAVC

//------------------------------------------------------
#pragma mark - ViewController LifeCycle
//------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"LoanContactCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LoanContactCell"];
   
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonInputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"MonthlyFeesInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MonthlyFeesInputCell"];
    
    
     [self.tableView registerNib:[UINib nibWithNibName:@"CommonActionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonActionCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EmptyHeaderView class]) bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:NSStringFromClass([EmptyHeaderView class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([OptionalFieldHeader class]) bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:NSStringFromClass([OptionalFieldHeader class])];
    
    agentModificationStatus = AgentModificationStatusNone;
    [self loadTableViewData];
 
    self.loanNoLabel.text=[NSString stringWithFormat:@"LOAN %@",self.loanNo];
}

-(CommonInputCell *)commonInputCell {
    if (!_commonInputCell) {
        _commonInputCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommonInputCell class])];
    }
    return _commonInputCell;
}
-(MonthlyFeesInputCell *)monthlyFeesInputCell {
    if (!_monthlyFeesInputCell) {
        _monthlyFeesInputCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MonthlyFeesInputCell class])];
    }
    return _monthlyFeesInputCell;
}

-(LoanContactCell *)loanContactCell {
    if (!_loanContactCell) {
        _loanContactCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LoanContactCell class])];
    }
    return _loanContactCell;
}
-(CommonActionCell *)commonActionCell {
    if (!_commonActionCell) {
        _commonActionCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommonActionCell class])];
    }
    return _commonActionCell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//------------------------------------------------------
#pragma mark - Button Actions
//------------------------------------------------------

-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}

-(IBAction)cellPhoneButtonTapAction:(id)sender{
    NSString *phoneNo;
    
    UIButton *button=(UIButton*)sender;
    AKDTableSection *sectionObj = self.tableData[3];
    
    phoneNo=[NSString stringWithFormat:@"tel:%@",((DLTableRow *)sectionObj.rowsData[button.tag]).rowValue];
   
    if ([phoneNo isEqualToString:@""]) {
        [AlertManager showAlertSingle:@"" msg:@"No phone no available."];
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
    }
}
-(IBAction)msgButtonTapAction:(id)sender{
    UIButton *button=(UIButton*)sender;
    AKDTableSection *sectionObj = self.tableData[3];
    NSString *email=[NSString stringWithFormat:@"%@",((DLTableRow *)sectionObj.rowsData[button.tag]).email];
    
    NSString *mailInfo = @"?subject= ";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@%@",email,mailInfo]]];
    NSLog(@"emailing");
    /*
    phoneNo=[NSString stringWithFormat:@"sms:%@",((DLTableRow *)sectionObj.rowsData[button.tag]).rowValue];
    
    if ([phoneNo isEqualToString:@""]) {
        [AlertManager showAlertSingle:@"" msg:@"No phone no available."];
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
    }*/
}

-(void)collapseButtonTapAction:(id)sender {
    UIButton *button=(UIButton*)sender;
    [self tableView:self.tableView toggleSection:button.tag];
}
//------------------------------------------------------
#pragma mark - API
//------------------------------------------------------

-(void) loadTableViewData{
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"LoanMobile" parameters:@{@"Loan":@"true",@"LoanNumber":self.loanNo} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get SettingsBrokerMobile : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                NSDictionary *infoDictionary=[responseObject objectForKey:@"Results"];
                
                // Loan Contact Section ------------------------------------------------------
                AKDTableSection *loanInofTopSection = [AKDTableSection new];
                loanInofTopSection.typeID = emptyHeader;
                loanInofTopSection.sectionHeaderHeight = 0;
                loanInofTopSection.isCollapsed=NO;
                NSMutableArray *rowArray=[NSMutableArray new];
                
                DLTableRow *dlTableRow=[DLTableRow new];
                dlTableRow.typeID=commonInputRow;
                dlTableRow.rowTitle=@"Purchase Price";
                double purchasePrice=[[[infoDictionary objectForKey:@"Loan"] objectForKey:@"PurchasePrice"] doubleValue];
                dlTableRow.rowValue=[NSString stringWithFormat:@"$%.02f",purchasePrice];
                dlTableRow.boolValue=YES;
                [rowArray addObject:dlTableRow];
                
                DLTableRow *dlTableRow1=[DLTableRow new];
                dlTableRow1.typeID=commonInputRow;
                dlTableRow1.rowTitle=@"Loan Amount";
                double loanAmount=[[[infoDictionary objectForKey:@"Loan"] objectForKey:@"PurchaseAmount"] doubleValue];
                dlTableRow1.rowValue=[NSString stringWithFormat:@"$%.02f",loanAmount];
                [rowArray addObject:dlTableRow1];
                
                DLTableRow *dlTableRow2=[DLTableRow new];
                dlTableRow2.typeID=commonInputRow;
                dlTableRow2.rowTitle=@"Down Payment";
                double DownPayment=[[[infoDictionary objectForKey:@"Loan"] objectForKey:@"DownPayment"] doubleValue];
                dlTableRow2.rowValue=[NSString stringWithFormat:@"$%.02f",DownPayment];
                
                [rowArray addObject:dlTableRow2];
                
                
                DLTableRow *dlTableRow3=[DLTableRow new];
                dlTableRow3.typeID=commonInputRow;
                dlTableRow3.rowTitle=@"Interest Rate";
                double InterestRate=[[[infoDictionary objectForKey:@"Loan"] objectForKey:@"InterestRate"] doubleValue];
                dlTableRow3.rowValue=[NSString stringWithFormat:@"$%.04f",InterestRate];
                [rowArray addObject:dlTableRow3];
                
                DLTableRow *dlTableRow4=[DLTableRow new];
                dlTableRow4.typeID=commonInputRow;
                dlTableRow4.rowTitle=@"Loan Term";
                double LoanTerm=[[[infoDictionary objectForKey:@"Loan"] objectForKey:@"LoanTerm"] doubleValue];
                dlTableRow4.rowValue=[NSString stringWithFormat:@"$%.02f",LoanTerm];
                [rowArray addObject:dlTableRow4];
                
                loanInofTopSection.rowsData = rowArray;
                
                //  Monthly Fees  Section ------------------------------------------------------
                AKDTableSection *monthlyFeesSection = [AKDTableSection new];
                monthlyFeesSection.typeID = monthlyFeesHeader;
                monthlyFeesSection.sectionInfo = @"Monthly Fees";
                monthlyFeesSection.sectionHeaderHeight = 44;
                monthlyFeesSection.isCollapsed=YES;
                monthlyFeesSection.inputValue=[[infoDictionary objectForKey:@"Loan"] valueForKey:@"MonthlyFees"];
                NSMutableArray *rowArray2=[NSMutableArray new];
                
                DLTableRow *dlTableRow20=[DLTableRow new];
                dlTableRow20.typeID=monthlyFeesRow;
                dlTableRow20.rowTitle=@"Other Financing P&I";
                dlTableRow20.boolValue=YES;
                double OtherFinancingPAndI=[[[infoDictionary objectForKey:@"Loan"] valueForKey:@"OtherFinancingPAndI"] doubleValue];
                dlTableRow20.rowValue=[NSString stringWithFormat:@"$%.02f",OtherFinancingPAndI];
                [rowArray2 addObject:dlTableRow20];
                
                DLTableRow *dlTableRow21=[DLTableRow new];
                dlTableRow21.typeID=monthlyFeesRow;
                dlTableRow21.rowTitle=@"Hazard Insurance";
                dlTableRow21.rowValue=[infoDictionary objectForKey:@"Hazard_Insurance"];
                double HazardInsurance=[[[infoDictionary objectForKey:@"Loan"] valueForKey:@"HazardInsurance"] doubleValue];
                dlTableRow21.boolValue=YES;
                dlTableRow21.rowValue=[NSString stringWithFormat:@"$%.02f",HazardInsurance];
                [rowArray2 addObject:dlTableRow21];
                
                DLTableRow *dlTableRow22=[DLTableRow new];
                dlTableRow22.typeID=monthlyFeesRow;
                dlTableRow22.rowTitle=@"Real Estate Taxes";
                double RealEstateTaxes=[[[infoDictionary objectForKey:@"Loan"] valueForKey:@"RealEstateTaxes"] doubleValue];
                dlTableRow22.boolValue=YES;
                dlTableRow22.rowValue=[NSString stringWithFormat:@"$%.02f",RealEstateTaxes];
                [rowArray2 addObject:dlTableRow22];
                
                DLTableRow *dlTableRow23=[DLTableRow new];
                dlTableRow23.typeID=monthlyFeesRow;
                dlTableRow23.rowTitle=@"Mortgage Insurance";
                double MortgageInsurance=[[[infoDictionary objectForKey:@"Loan"] valueForKey:@"MortgageInsurance"] doubleValue];
                dlTableRow23.rowValue=[NSString stringWithFormat:@"$%.02f",MortgageInsurance];
                [rowArray2 addObject:dlTableRow23];
                
                DLTableRow *dlTableRow24=[DLTableRow new];
                dlTableRow24.typeID=monthlyFeesRow;
                dlTableRow24.rowTitle=@"HOA Dues";
                dlTableRow24.boolValue=YES;
                double HOADues=[[[infoDictionary objectForKey:@"Loan"] valueForKey:@"HOADues"] doubleValue];
                dlTableRow24.rowValue=[NSString stringWithFormat:@"$%.02f",HOADues];
                [rowArray2 addObject:dlTableRow24];
                
                DLTableRow *dlTableRow25=[DLTableRow new];
                dlTableRow25.typeID=monthlyFeesRow;
                dlTableRow25.rowTitle=@"Other Expenses";
                dlTableRow25.boolValue=YES;
                double OtherExpenses=[[[infoDictionary objectForKey:@"Loan"] valueForKey:@"OtherExpenses"] doubleValue];
                dlTableRow25.rowValue=[NSString stringWithFormat:@"$%.02f",OtherExpenses];
                [rowArray2 addObject:dlTableRow25];
                
                monthlyFeesSection.rowsData = rowArray2;
                
                // Max Monthly Payment Section ------------------------------------------------------
                AKDTableSection *monthlyPaymentSection = [AKDTableSection new];
                monthlyPaymentSection.typeID = emptyHeader;
                monthlyPaymentSection.sectionInfo = @"";
                monthlyPaymentSection.sectionHeaderHeight = 0;
                monthlyPaymentSection.isCollapsed=NO;
                NSMutableArray *rowArray3=[NSMutableArray new];
                
                DLTableRow *dlTableRow30=[DLTableRow new];
                dlTableRow30.typeID=commonInputRow;
                dlTableRow30.rowTitle=@"Monthly Payment";
                double montlyPayment=[[[infoDictionary objectForKey:@"Loan"] valueForKey:@"MonthlyPayment"] doubleValue];
                dlTableRow30.rowValue=[NSString stringWithFormat:@"$%.02f",montlyPayment];
                [rowArray3 addObject:dlTableRow30];
                
                DLTableRow *dlTableRow31=[DLTableRow new];
                dlTableRow31.typeID=commonActionRow;
                dlTableRow31.rowTitle=@"";
                [rowArray3 addObject:dlTableRow31];
                
                monthlyPaymentSection.rowsData = rowArray3;
                
                
                AKDTableSection *contactSection = [AKDTableSection new];
                contactSection.typeID = loanContactHeader;
                contactSection.sectionInfo = @"Loan Contacts";
                contactSection.sectionHeaderHeight =44;
                contactSection.isCollapsed=NO;
                
                NSMutableArray *rowArray4=[NSMutableArray new];
                NSMutableArray *contactArray=[LoanContactInfoBBA arrayOfModelsFromDictionaries:responseObject[@"Contacts"]];
                for (LoanContactInfoBBA *loanContact in contactArray) {
                    DLTableRow *dlTableRow40=[DLTableRow new];
                    dlTableRow40.typeID=loanContactRow;
                    dlTableRow40.rowTitle=loanContact.Name;
                    dlTableRow40.rowValue=loanContact.PhoneNumber;
                    [rowArray4 addObject:dlTableRow40];
                    
                }/*
                LoanContactInfoBBA *loanContact=[LoanContactInfoBBA new];
                loanContact.Name=@"Dulal Hossain";
                loanContact.Email=@"dulal0026@gmail.com";
                loanContact.PhoneNumber=@"01710097586";
                DLTableRow *dlTableRow40=[DLTableRow new];
                dlTableRow40.typeID=loanContactRow;
                dlTableRow40.rowTitle=loanContact.Name;
                dlTableRow40.rowValue=loanContact.PhoneNumber;
                [rowArray4 addObject:dlTableRow40];
                
                LoanContactInfoBBA *loanContact1=[LoanContactInfoBBA new];
                loanContact1.Name=@"Uzzal Hossain";
                loanContact1.Email=@"dulal_026@yahoo.com";
                loanContact1.PhoneNumber=@"01916043646";
                DLTableRow *dlTableRow41=[DLTableRow new];
                dlTableRow41.typeID=loanContactRow;
                dlTableRow40.rowValue=loanContact1.PhoneNumber;
                dlTableRow41.rowTitle=loanContact1.Name;
                [rowArray4 addObject:dlTableRow41];
                contactSection.rowsData = rowArray4;*/
                
                self.tableData = [@[
                                    loanInofTopSection,
                                    monthlyFeesSection,
                                    monthlyPaymentSection,
                                    contactSection
                                    ] mutableCopy];
                
                self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                [self.tableView reloadData];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[responseObject objectForKey:@"General"] valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

-(void)saveButtonTapAction:(id)sender
{
    AKDTableSection *accountSectionObj = self.tableData[0];
    AKDTableSection *monthlyFeesSectionObj = self.tableData[1];
    
    if ([InputValidator isLoanInfoValidBBA:((DLTableRow *)accountSectionObj.rowsData[0]).rowValue otherFinacing:((DLTableRow *)monthlyFeesSectionObj.rowsData[0]).rowValue hazardInsurance:((DLTableRow *)monthlyFeesSectionObj.rowsData[1]).rowValue realStateTaxes:((DLTableRow *)monthlyFeesSectionObj.rowsData[2]).rowValue HOAFees:((DLTableRow *)monthlyFeesSectionObj.rowsData[4]).rowValue otherExpenses:((DLTableRow *)monthlyFeesSectionObj.rowsData[5]).rowValue]) {
        [self requestToUpdateAccountInfo:accountSectionObj monthlySection:monthlyFeesSectionObj];
    }
}
-(void)requestToUpdateAccountInfo:(AKDTableSection *)accountSectionObj monthlySection:(AKDTableSection*)monthlyFeesSectionObj
{
    NSDictionary *param = @{
                            @"Save":@"true",
                            @"Loan_Number" : self.loanNo,
                            @"Max_Purchase_Price" : ((DLTableRow *)monthlyFeesSectionObj.rowsData[0]).rowValue,
                            @"Other_Financing_PI" : ((DLTableRow *)monthlyFeesSectionObj.rowsData[0]).rowValue,
                            @"Hazard_Insurance" : ((DLTableRow *)monthlyFeesSectionObj.rowsData[1]).rowValue,
                            @"Real_Estate_Taxes" : ((DLTableRow *)monthlyFeesSectionObj.rowsData[2]).rowValue,
                            @"HOA_Dues" : ((DLTableRow *)monthlyFeesSectionObj.rowsData[4]).rowValue,
                            @"Other_Expenses" : ((DLTableRow *)monthlyFeesSectionObj.rowsData[5]).rowValue
                            };
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"LoanMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Loan Successfully updated."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void)requestToDownloadPrequal
{
    NSDictionary *param = @{
                            @"DownloadPrequal":@"true",
                            @"loan_Number" : self.loanNo
                            };
    [SVProgressHUD showWithStatus:@"Downloading File..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"LoanMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (operation.response.statusCode != 200) {
            [SVProgressHUD showErrorWithStatus:@"File Not Saved."];
        }else{
            NSData *data = operation.responseData;
            //[(NSHTTPURLResponse*)[operation response] allHeaderFields][@"Content-Disposition"]
            NSString *serverPdfFullName =[(NSHTTPURLResponse*)[operation response] allHeaderFields][@"Content-Disposition"];
            NSArray *tempArray=[serverPdfFullName componentsSeparatedByString:@"filename="];
            NSMutableString *tempString;
            if (tempArray.count!=0) {
                tempString=[[tempArray objectAtIndex:1] mutableCopy];
            }
            [tempString replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:(NSRange){0,[tempString length]}];
            
            NSString *pdfName=[NSString stringWithFormat:@"%@.pdf",tempString];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:pdfName];
            
            [data writeToFile:path atomically:YES];
            [self previewDocument:[NSURL fileURLWithPath:path]];
            [SVProgressHUD dismiss];
        }
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            
            NSLog(@"Download = %f", (float)totalBytesRead / totalBytesExpectedToRead);
            
        }];
        [operation start];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

-(void)requestToDownloadLSU
{
    NSDictionary *param = @{
                            @"downloadlsu":@"true",
                            @"loannumber" : self.loanNo
                            };
    [SVProgressHUD showWithStatus:@"Downloading File..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"loanmobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        if (operation.response.statusCode != 200) {
            [SVProgressHUD showErrorWithStatus:@"File Not Saved."];
        }else{
           NSData *data = operation.responseData;
           //[(NSHTTPURLResponse*)[operation response] allHeaderFields][@"Content-Disposition"]
            NSString *serverPdfFullName =[(NSHTTPURLResponse*)[operation response] allHeaderFields][@"Content-Disposition"];
            NSArray *tempArray=[serverPdfFullName componentsSeparatedByString:@"filename="];
            NSMutableString *tempString;
            if (tempArray.count!=0) {
                tempString=[[tempArray objectAtIndex:1] mutableCopy];
            }
            [tempString replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:(NSRange){0,[tempString length]}];
        
            NSString *pdfName=[NSString stringWithFormat:@"%@.pdf",tempString];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:pdfName];
        
            [data writeToFile:path atomically:YES];
           
            [self previewDocument:[NSURL fileURLWithPath:path]];
           // NSURL *URL = [[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"pdf"];
            //[self previewDocument:URL];
            [SVProgressHUD dismiss];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

//------------------------------------------------------
#pragma mark - TableView Delegate/DataSource
//------------------------------------------------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.tableData count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.isCollapsed? 0 : [sectionObj.rowsData count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.sectionHeaderHeight;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IOS8_OR_ABOVE) {
        return UITableViewAutomaticDimension;
    }
    
    UITableViewCell *cell = nil;
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
    
    switch (dLTableRow.typeID)
    {
        case commonActionRow:
            cell = self.commonActionCell;
            break;
            
        case commonInputRow:
            cell = self.commonInputCell;
            break;
            
        case monthlyFeesRow:
            cell = self.monthlyFeesInputCell;
            break;
            
        case loanContactRow:
            cell = self.loanContactCell;
            break;
            
        default:
            break;
    }
    
    [cell updateConstraintsIfNeeded];
    [cell layoutIfNeeded];
    
    return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    AKDTableSection *sectionObj = self.tableData[section];
    
    if (sectionObj.typeID == emptyHeader) {

        AccountHeader *optionHeader = [AccountHeader instanceWithNibName:nil bundle:nil owner:nil];
        if (optionHeader) {
            optionHeader.collapseButton.hidden=YES;
            optionHeader.backView.layer.shadowOffset = CGSizeMake(5, 0);
            optionHeader.titleLabel.text=sectionObj.sectionInfo;
            optionHeader.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            optionHeader.backView.layer.shadowOpacity = 1;
            optionHeader.backView.layer.shadowRadius = 1.0;
            optionHeader.collapseButton.tag=section;
            optionHeader.titleLabel.textColor=[UIColor colorWithRed:60.0/255 green:99.0/255 blue:170.0/255 alpha:1.0];
        }
        headerView = optionHeader;
    }
    else if (sectionObj.typeID == monthlyFeesHeader) {
        OptionalFieldHeader *optionHeader = [OptionalFieldHeader instanceWithNibName:nil bundle:nil owner:nil];
        if (optionHeader) {
            optionHeader.backgroundColor=[UIColor redColor];
            optionHeader.inputTextField.hidden=NO;
            optionHeader.inputTextField.userInteractionEnabled=NO;
            optionHeader.inputTextField.text=[NSString stringWithFormat:@"%@",sectionObj.inputValue];
            optionHeader.backView.layer.shadowOffset = CGSizeMake(5, 0);
            optionHeader.titleLabel.text=sectionObj.sectionInfo;
            optionHeader.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            optionHeader.backView.layer.shadowOpacity = 1;
            optionHeader.backView.layer.shadowRadius = 1.0;
            optionHeader.collapseButton.tag=section;
            [optionHeader.collapseButton addTarget:self action:@selector(collapseButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        headerView = optionHeader;
    }
    
    else if (sectionObj.typeID == loanContactHeader) {
        OptionalFieldHeader *optionHeader = [OptionalFieldHeader instanceWithNibName:nil bundle:nil owner:nil];
        if (optionHeader) {
            optionHeader.backgroundColor=[UIColor redColor];
            optionHeader.inputTextField.hidden=YES;
           // optionHeader.inputTextField.text=[NSString stringWithFormat:@"%@",sectionObj.inputValue];
            optionHeader.backView.layer.shadowOffset = CGSizeMake(5, 0);
            optionHeader.titleLabel.text=sectionObj.sectionInfo;
            optionHeader.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            optionHeader.backView.layer.shadowOpacity = 1;
            optionHeader.backView.layer.shadowRadius = 1.0;
            optionHeader.collapseButton.tag=section;
            [optionHeader.collapseButton addTarget:self action:@selector(collapseButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        headerView = optionHeader;
    }
    else if (sectionObj.typeID == emptyHeader) {
        EmptyHeaderView *dView = [EmptyHeaderView instanceWithNibName:nil bundle:nil owner:nil];
        if (dView) {
            //            dView
        }
        headerView = dView;
    }
    return headerView;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
    static NSString *identifier;

    if (dLTableRow.typeID==commonInputRow) {
        identifier = @"CommonInputCell";
        CommonInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=[NSString stringWithFormat:@"%@",dLTableRow.rowValue];
        cell.inputField.userInteractionEnabled=dLTableRow.boolValue;
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.inputField.keyboardType=UIKeyboardTypeNumberPad;
        if (![cell.inputField.allTargets containsObject:self]) {
            [cell.inputField addTarget:self action:@selector(textFieldTextChangedOnAccount:) forControlEvents:UIControlEventEditingChanged];
        }
        return cell;
    }
    else if (dLTableRow.typeID==monthlyFeesRow) {
        identifier = @"MonthlyFeesInputCell";
        MonthlyFeesInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=[NSString stringWithFormat:@"%@",dLTableRow.rowValue];
        
       cell.bgView.backgroundColor=[UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1.0];
        cell.inputField.userInteractionEnabled=dLTableRow.boolValue;
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.inputField.keyboardType=UIKeyboardTypeNumberPad;
        if (![cell.inputField.allTargets containsObject:self]) {
            [cell.inputField addTarget:self action:@selector(textFieldTextChangedOnMonthlyFees:) forControlEvents:UIControlEventEditingChanged];
        }
        return cell;
    }
    
    else  if (dLTableRow.typeID==commonActionRow)
    {
        identifier = @"CommonActionCell";
        CommonActionCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 5);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.actionButton.tag=indexPath.section;
        
        if (![cell.actionButton.allTargets containsObject:self]) {
            [cell.actionButton addTarget:self action:@selector(saveButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        return cell;
    }
    else {
        identifier = @"LoanContactCell";
        LoanContactCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=[NSString stringWithFormat:@"%@", dLTableRow.rowValue];
        
        // if (sectionObj.typeID==monthlyExpensesHeader) cell.bgView.backgroundColor=[UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1.0];
        
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.cellPhoneButton.tag=indexPath.row;
        cell.msgButton.tag=indexPath.row;
        
        if (![cell.cellPhoneButton.allTargets containsObject:self]) {
            [cell.cellPhoneButton addTarget:self action:@selector(cellPhoneButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (![cell.msgButton.allTargets containsObject:self]) {
            [cell.msgButton addTarget:self action:@selector(msgButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

//------------------------------------------------------
#pragma mark - TableView Utils (Private)
//------------------------------------------------------

-(void) tableView:(UITableView *) tableView toggleSection:(NSInteger) section {
    AKDTableSection *sectionObj = self.tableData[section];
    sectionObj.isCollapsed = !sectionObj.isCollapsed;
    
    [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
}

//------------------------------------------------------
#pragma mark - Segment Cotrol TapAction
//------------------------------------------------------

- (IBAction)segmentControlTapAction:(id)sender {
  
    if (self.segmentControl.selectedSegmentIndex==0) {
        //PreQual Section
        [self requestToDownloadPrequal];
    }
    else{
       //LSU Section
        [self requestToDownloadLSU];
        //[self openDocument:sender];
    }
}
#pragma mark - UITextField Value change Inspector
-(void) textFieldTextChangedOnAccount:(UITextField *)sender
{
    CommonInputCell *cell = (CommonInputCell *)[sender firstSuperviewOfClass:[CommonInputCell class]];
    if (cell) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        AKDTableSection *sectionObj = self.tableData[indexPath.section];
        DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
        dLTableRow.rowValue = sender.text;
    }
}
-(void) textFieldTextChangedOnMonthlyFees:(UITextField *)sender
{
    MonthlyFeesInputCell *cell = (MonthlyFeesInputCell *)[sender firstSuperviewOfClass:[MonthlyFeesInputCell class]];
    if (cell) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        AKDTableSection *sectionObj = self.tableData[indexPath.section];
        DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
        dLTableRow.rowValue = sender.text;
    }
}


- (void)previewDocument:(NSURL*)URL {
   // NSURL *URL = [[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"pdf"];
    
    if (URL) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [self.documentInteractionController setDelegate:self];
        
        // Preview PDF
        [self.documentInteractionController presentPreviewAnimated:YES];
    }
}
- (IBAction)openDocument:(id)sender {
    UIButton *button = (UIButton *)sender;
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"pdf"];
    
    if (URL) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [self.documentInteractionController setDelegate:self];
        
        // Present Open In Menu
        [self.documentInteractionController presentOpenInMenuFromRect:[button frame] inView:self.view animated:YES];
    }
}
- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}
@end
