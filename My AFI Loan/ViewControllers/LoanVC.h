//
//  ViewController.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonItemCell.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"
#import "ApplicationTextManager.h"
#import "LoanInfo.h"
#import "LoanDetailsVC.h"
#import "LoanDetailsBBAVC.h"

@interface LoanVC : UIViewController<UIAlertViewDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addLoanButton;
@property(nonatomic,strong)NSArray *loanArray;
@property(nonatomic,strong)NSString *sessionId;
@property(nonatomic,strong)NSString *valueOfSessionID;

- (IBAction)addLoanButtonTapAction:(id)sender;
@end

