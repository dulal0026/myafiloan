//
//  AgentDetailsVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "AgentDetailsVC.h"

@interface AgentDetailsVC ()

@end

@implementation AgentDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    self.agentImageView.layer.cornerRadius=2.0f;
    self.agentImageView.layer.borderColor=[[UIColor colorWithRed:218.0/255 green:217.0/255 blue:226.0/255 alpha:1.0]CGColor];
    self.agentNameLabel.text=self.agentInfo.Display_Name;
    NSString *statusText;
    if (self.agentInfo.Active){
        statusText=@"Active";
        self.headerView.layer.shadowOffset = CGSizeMake(5, 5);
    }
    else{
        self.tableHeaderHeightConstraint.constant=0;
        self.headerView.layer.shadowOffset = CGSizeMake(5, 0);
     statusText=@"Pending";
    }
    self.headerView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
    self.headerView.layer.shadowOpacity = 1;
    self.headerView.layer.shadowRadius = 1.0;
    
    self.agentStatusLabel.text=statusText;
    NSString *imgURL =[NSString stringWithFormat:@"%@",self.agentInfo.Photo_URL];
    [self.agentImageView  setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"pic_holder"]];
 
    [self filledAgentInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}
-(void)filledAgentInfo{
    
}
-(void)reinviteButtonTapAction:(id)sender{
    NSLog(@"reinviteButtonTapAction");
    [self reguestToArchiveAgent:self.agentInfo.Email];
}
#pragma mark - API Methods
-(void) reguestToArchiveAgent:(NSString *)string{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    NSDictionary *param = @{
                            @"Archive":@"true",
                            @"Email" :string
                            };
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"AgentsMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Agent Successfully Archived."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

#pragma mark - TableView Delegate/DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.agentInfo.Active? 7 : 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.agentInfo.Active) {
        AgentDetailsCell *agentDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"AgentDetailsCell"];
        agentDetailsCell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        agentDetailsCell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        agentDetailsCell.bgView.layer.shadowOpacity = 1;
        agentDetailsCell.bgView.layer.shadowRadius = 1.0;
        
        if (indexPath.row==0) {
            agentDetailsCell.titleLabel.text=@"First Name";
            agentDetailsCell.valueLabel.text=self.agentInfo.First_Name;
        }
        else if (indexPath.row==1) {
            agentDetailsCell.titleLabel.text=@"Last Name";
            agentDetailsCell.valueLabel.text=self.agentInfo.Last_Name;
        }
        else if (indexPath.row==2) {
            agentDetailsCell.titleLabel.text=@"Company";
            agentDetailsCell.valueLabel.text=self.agentInfo.Company;
        }
        else if (indexPath.row==3) {
            agentDetailsCell.titleLabel.text=@"Address";
            agentDetailsCell.valueLabel.text=self.agentInfo.Address;
        }
        else if (indexPath.row==4) {
            agentDetailsCell.titleLabel.text=@"Email";
            agentDetailsCell.valueLabel.text=self.agentInfo.Email;
        }
        else if (indexPath.row==5) {
            agentDetailsCell.titleLabel.text=@"Office Phone";
            agentDetailsCell.valueLabel.text=self.agentInfo.OfficePhone;
        }
        else if (indexPath.row==6) {
            agentDetailsCell.titleLabel.text=@"Mobile Phone";
            agentDetailsCell.valueLabel.text=self.agentInfo.MobilePhone;
            
        }
        return agentDetailsCell;
    }
    else{
       ReinviteCell *reinviteCell=[tableView dequeueReusableCellWithIdentifier:@"ReinviteCell"];
        reinviteCell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        reinviteCell.bgView.layer.shadowOffset = CGSizeMake(5, 5);
        reinviteCell.bgView.layer.shadowOpacity = 1;
        reinviteCell.bgView.layer.shadowRadius = 1.0;
        [reinviteCell.reinviteButton addTarget:self action:@selector(reinviteButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        return reinviteCell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   // [self performSegueWithIdentifier:@"agentDetailsSegue" sender:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.agentArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}
@end
