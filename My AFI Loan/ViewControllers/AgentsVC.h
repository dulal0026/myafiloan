//
//  AgentsVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFILoanRequestManager.h"
#import "ApplicationTextManager.h"
#import "AppUserDefault.h"
#import "AgentInfo.h"
#import "AgentsCell.h"
#import "UIImageView+AFNetworking.h"
#import "UIViewController+MMDrawerController.h"
#import "AgentDetailsVC.h"
#import "AlertManager.h"
#import "SVProgressHUD.h"
#import "InputValidator.h"

@interface AgentsVC : UIViewController<UITableViewDelegate,UIAlertViewDelegate>
@property(nonatomic,strong)NSMutableArray *agentArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
-(IBAction)showMenu:(id)sender;
-(IBAction)inviteAgent:(id)sender;
@end
