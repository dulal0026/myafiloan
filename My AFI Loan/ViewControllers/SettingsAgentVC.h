//
//  SettingsAgentVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "AKDTableSection.h"
#import "PhotoHeaderView.h"
#import "CommonInputCell.h"
#import "CommonActionCell.h"
#import "ViewUtils.h"
#import "EmptyHeaderView.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"
#import "Constants.h"
#import "ApplicationTextManager.h"
#import "DLTableRow.h"
#import "UtilityManager.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"

@interface SettingsAgentVC : UIViewController <UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
-(IBAction)showMenu:(id)sender;
@end
