//
//  Constants.h

//
//  Created by Dulal on 10/07/13.
//  Copyright (c) 2013 DEVP-33. All rights reserved.
//


#define buttonEnableColor (UIColor *)[UIColor colorWithRed:224.0/255.0 green:102.0/255.0 blue:98.0/255.0 alpha:1.0]
#define buttonDisabledColor (UIColor *)[UIColor colorWithRed:60.0/255.0 green:54.0/255.0 blue:57.0/255.0 alpha:1.0]

#define buttonEnableTextColor (UIColor *)[UIColor whiteColor]
#define buttonDisabledTextColor (UIColor *)[UIColor colorWithRed:81.0/255.0 green:78.0/255.0 blue:79.0/255.0 alpha:1.0]

//iOS Version
#define IS_IOS_7_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_IOS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

typedef NS_ENUM(NSInteger, AgentModificationStatus) {
    AgentModificationStatusNone,
    AgentModificationStatusAdd,
    AgentModificationStatusRemove,
    AgentModificationStatusPrimary
};

#define expandCloseImage (UIImage *)[UIImage imageNamed:@"arrow_upper"]
#define expandImage (UIImage *)[UIImage imageNamed:@"down_arrow"]


