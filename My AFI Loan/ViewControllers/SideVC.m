//
//  ViewController.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "SideVC.h"

@interface SideVC (){
    NSInteger userType;
}
@property (copy, nonatomic) NSArray *menus;
@end

@implementation SideVC

- (void)viewDidLoad {
    [super viewDidLoad];
    userType=[[AppUserDefault sharedInstance] getIntegerValueforKey:userTypeKey];
    if (userType==1) self.menus = @[@"LOANS",@"AGENTS",@"SETTINGS",@"LOG OUT"];
    else self.menus = @[@"LOANS",@"NOTIFICATIONS",@"SETTINGS",@"LOG OUT"];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.menus count];
}
/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *loggedInUserArray=[[[CoreDBManager sharedInstance]getLoggedInUserArray] valueForKey:@"createUserInfo"];
    NSInteger tableCounter;
    if (loggedInUserArray.count!=0) {
        tableCounter=[self.menus count];
    }
    else tableCounter=[self.menus count]-1;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    return (screenHeight-_upperView.frame.size.height)/tableCounter-10;
}*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AAPMenuCell *cell = (AAPMenuCell *)[tableView dequeueReusableCellWithIdentifier:@"SIDECELL" forIndexPath:indexPath];
    /*
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:41.0/256.0 green:41.0/256.0 blue:41.0/256.0 alpha:1];

    bgColorView.layer.masksToBounds = YES;
    [cell setSelectedBackgroundView:bgColorView];
    cell.backView.layer.cornerRadius=3.0f;*/
    cell.titleLabel.text = self.menus[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0){
        UINavigationController *navCont=(UINavigationController*)self.mm_drawerController.centerViewController;
        [navCont popToRootViewControllerAnimated:NO];
        ///[navCont.viewControllers[0] performSegueWithIdentifier:segueIdentifier sender:self];
        [self.mm_drawerController closeDrawerAnimated:YES completion:NULL];
    }
    else if (indexPath.row==1){
        if (userType==1) [self performNavigation:@"agentsSegue"];
        else [self performNavigation:@"notificationSegue"];
    }
    else if (indexPath.row==2){
        if (userType==1) [self performNavigation:@"SettingsLoanOfficerSegue"];
        else if (userType==2)[self performNavigation:@"SettingsAgentSegue"];
        else if (userType==3)[self performNavigation:@"SettingsBorrowerSegue"];
        else if (userType==5)[self performNavigation:@"SettingsBrokerSegue"];
    }
    else if (indexPath.row==3){
        [[AppUserDefault sharedInstance] removeUserDefaultValue:sessionKey];
        [[AppUserDefault sharedInstance] removeUserDefaultValue:userTypeKey];
        [self performNavigation:@"LoginSegue"];
    }
}
-(void)performNavigation:(NSString*)segueIdentifier{
    UINavigationController *navCont=(UINavigationController*)self.mm_drawerController.centerViewController;
    [navCont popToRootViewControllerAnimated:NO];
    [navCont.viewControllers[0] performSegueWithIdentifier:segueIdentifier sender:self];
    [self.mm_drawerController closeDrawerAnimated:YES completion:NULL];
}
@end

@implementation AAPMenuCell

@end
