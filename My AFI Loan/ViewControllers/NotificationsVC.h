//
//  NotificationsVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/16/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"

@interface NotificationsVC : UIViewController
- (IBAction)showMenu:(id)sender;
@property(nonatomic,strong)NSMutableArray *notificationArray;
@property (weak, nonatomic) IBOutlet UITableView *notificaitonTableView;
@end
@interface NotificationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *borrowerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificaitonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *borrowerImageView;
@end
