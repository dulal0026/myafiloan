//
//  SettingsLoanOfficerVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "SettingsLoanOfficerVC.h"

typedef NS_ENUM(NSInteger, SectionRowType) {
    CommonInputRow,
    CommonActionRow
};
@interface SettingsLoanOfficerVC ()

@end

@implementation SettingsLoanOfficerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerView.layer.shadowOffset = CGSizeMake(5, 5);
    self.headerView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
    self.headerView.layer.shadowOpacity = 1;
    self.headerView.layer.shadowRadius = 1.0;
    [self getLoanOfficerSettingData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(IBAction)uploadPhotoButtonAction:(id)sender{
    NSLog(@"upload your photo");
    
    UIActionSheet *aSheet = [[UIActionSheet alloc] initWithTitle:@"Select option:"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:@"Use Camera",@"Choose From Camera-Roll", nil];
    [aSheet showInView:self.view];
}
-(void)saveButtonTapAction:(id)sender{
    NSLog(@"Save button tapped");
    if ([InputValidator isLoanOfficerInfoValid:((DLTableRow*)[self.tableData objectAtIndex:0]).rowValue facebook:((DLTableRow*)[self.tableData objectAtIndex:1]).rowValue twitter:((DLTableRow*)[self.tableData objectAtIndex:2]).rowValue linkedIn:((DLTableRow*)[self.tableData objectAtIndex:0]).rowValue]) {
        
         [self requestToUpdateAccountInfo];
    }
   
}
//------------------------------------------------------
#pragma mark - API -
#pragma mark - Api methods
//------------------------------------------------------
-(void) getLoanOfficerSettingData {
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsLoanOfficerMobile" parameters:@{@"Settings":@"true"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get SettingsLoanOfficer : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                NSDictionary *infoDictionary=[responseObject objectForKey:@"Results"];
              
                NSString *imgURL=[infoDictionary valueForKey:@"PicturePath"];
            
                NSString* encodedImageUrl = [imgURL stringByAddingPercentEscapesUsingEncoding:
                                             NSUTF8StringEncoding];
                
                [self.imageView sd_setImageWithURL:[NSURL URLWithString:encodedImageUrl]
                                         placeholderImage:[UIImage imageNamed:@"pic_holder"]];
                self.tableData=[NSMutableArray new];
                DLTableRow *dlTableRow0=[DLTableRow new];
                dlTableRow0.typeID=CommonInputRow;
                dlTableRow0.rowTitle=@"Website";
                dlTableRow0.rowValue=[infoDictionary valueForKey:@"Website"];
                [self.tableData addObject:dlTableRow0];
                
                DLTableRow *dlTableRow1=[DLTableRow new];
                dlTableRow1.typeID=CommonInputRow;
                dlTableRow1.rowTitle=@"Facebook";
                dlTableRow1.rowValue=[infoDictionary valueForKey:@"Facebook"];
                [self.tableData addObject:dlTableRow1];
                
                DLTableRow *dlTableRow2=[DLTableRow new];
                dlTableRow2.typeID=CommonInputRow;
                dlTableRow2.rowTitle=@"Twitter";
                dlTableRow2.rowValue=[infoDictionary valueForKey:@"Twitter"];
                [self.tableData addObject:dlTableRow2];
                
                DLTableRow *dlTableRow3=[DLTableRow new];
                dlTableRow3.typeID=CommonInputRow;
                dlTableRow3.rowTitle=@"LinkedIn";
                dlTableRow3.rowValue=[infoDictionary valueForKey:@"LinkedIn"];
                [self.tableData addObject:dlTableRow3];
                
                DLTableRow *dlTableRow4=[DLTableRow new];
                dlTableRow4.typeID=CommonActionRow;
                dlTableRow4.rowTitle=@"";
                dlTableRow4.rowValue=@"";
                [self.tableData addObject:dlTableRow4];
                
                [self.tableView reloadData];
                
                self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[responseObject objectForKey:@"General"] valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void)requestToUpdateAccountInfo
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    NSDictionary *param = @{
                            @"UpdateAccountFooter":@"True",
                            @"Website" : ((DLTableRow*)[self.tableData objectAtIndex:0]).rowValue,
                            @"Facebook" : ((DLTableRow*)[self.tableData objectAtIndex:1]).rowValue,
                            @"Twitter" : ((DLTableRow*)[self.tableData objectAtIndex:2]).rowValue,
                            @"LinkedIn" : ((DLTableRow*)[self.tableData objectAtIndex:3]).rowValue
                            };

    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsLoanOfficerMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Account Info : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Information Successfully updated."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
#pragma mark - UITextField Value change Inspector
-(void) textFieldTextChanged:(UITextField *)sender
{
    if ([sender firstSuperviewOfClass:[AgentDetailsCell class]])
    {
        AgentDetailsCell *cell = (AgentDetailsCell *)[sender firstSuperviewOfClass:[AgentDetailsCell class]];
        if (cell) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            DLTableRow *dLTableRow = self.tableData[indexPath.row];
            dLTableRow.rowValue = sender.text;
        }
    }
}

#pragma mark - TableView Delegate/DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLTableRow *dLTableRow = self.tableData[indexPath.row];
    if (dLTableRow.typeID==CommonInputRow) {
        AgentDetailsCell *agentDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"AgentDetailsCell"];
        agentDetailsCell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        agentDetailsCell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        agentDetailsCell.bgView.layer.shadowOpacity = 1;
        agentDetailsCell.bgView.layer.shadowRadius = 1.0;
        agentDetailsCell.titleLabel.text=dLTableRow.rowTitle;
        agentDetailsCell.inputTextField.text=dLTableRow.rowValue;
        if (![agentDetailsCell.inputTextField.allTargets containsObject:self]) {
            [agentDetailsCell.inputTextField addTarget:self action:@selector(textFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
        }
        return agentDetailsCell;
    }
    else{
        ReinviteCell *reinviteCell=[tableView dequeueReusableCellWithIdentifier:@"ReinviteCell"];
        reinviteCell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        reinviteCell.bgView.layer.shadowOffset = CGSizeMake(5, 5);
        reinviteCell.bgView.layer.shadowOpacity = 1;
        reinviteCell.bgView.layer.shadowRadius = 1.0;
        [reinviteCell.reinviteButton addTarget:self action:@selector(saveButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        return reinviteCell;
    }
}

#pragma mark - uploadPhoto
-(void)uploadPhoto:(UIImage *)image
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    AFHTTPRequestOperation *op = [[AFILoanRequestManager manager] POST:@"SettingsLoanOfficerMobile" parameters:@{@"UpdatePhoto":@"true"} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData
                                    name:@"photo"
                                fileName:@"photo.jpeg"
                                mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *JSON = (NSDictionary *)responseObject;
        NSLog(@"responseObject = %@",JSON);
        [SVProgressHUD showSuccessWithStatus:@"Photo Successfully uploaded."];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
    
    [op setUploadProgressBlock:^(NSUInteger bytesWritten,
                                 long long totalBytesWritten,
                                 long long totalBytesExpectedToWrite) {
        NSLog(@"Wrote %ld/%ld", (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
    }];
    
    [op start];
}

//=================================================================
#pragma mark - ActionSheet Delegates
//=================================================================
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self openWithSourceType:UIImagePickerControllerSourceTypeCamera];
            break;
        case 1:
            [self openWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        case 2:
            NSLog(@"Cancel.");
            break;
            
        default:
            break;
    }
}

-(void)openWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    pickerController.allowsEditing = NO;
    pickerController.sourceType = sourceType;
    pickerController.modalPresentationStyle = UIModalPresentationFullScreen;
    
    if (IS_IOS_8_OR_LATER) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self presentViewController:pickerController animated:YES completion:nil];
        }];
    }
    else
        [self presentViewController:pickerController animated:YES completion:nil];
}

//=================================================================
#pragma mark - ImagePicker Delegates
//=================================================================

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.imageView.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self uploadPhoto:chosenImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

@end
