//
//  LoanDetailsVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/16/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "AKDTableSection.h"
#import "LoanContactCell.h"
#import "OptionalFieldHeader.h"
#import "DLTableRow.h"
#import "ViewUtils.h"
#import "AgentCell.h"
#import "Constants.h"
#import "ApplicationTextManager.h"
#import "CommonInputCell.h"
#import "CommonInputSwitchCell.h"
#import "AFILoanRequestManager.h"
#import "SVProgressHUD.h"
#import "AppUserDefault.h"
#import "ApplicationTextManager.h"
#import "AlertManager.h"
#import "DisabledInputCell.h"
#import "MonthlyFeesInputCell.h"
#import "CommonActionCell.h"
#import "GraySwitchCell.h"
#import "LoanContactInfoBBA.h"
#import "InputValidator.h"
#import "NSString+Utilities.h"


@interface LoanDetailsVC : UIViewController <SWTableViewCellDelegate,UIAlertViewDelegate>

@property(nonatomic,strong)NSString *loanNo;
@property(nonatomic,strong)IBOutlet UILabel *loanNoLabel;
-(IBAction)goBack:(id)sender;
-(IBAction)showMenu:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
- (IBAction)segmentControlTapAction:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@end
