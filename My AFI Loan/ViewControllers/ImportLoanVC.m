//
//  ImportLoanVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/29/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "ImportLoanVC.h"
#import "SVProgressHUD.h"

@interface ImportLoanVC (){
}

@end

@implementation ImportLoanVC

//_________________________________________________________________________
#pragma mark - Viewcontroller Lifecycle
//_________________________________________________________________________

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonItemCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"itemCell"];
    
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//_________________________________________________________________________
#pragma mark - Button Actions
//_________________________________________________________________________

-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)importSelectedLoan:(UIButton*)sender {
    NSLog(@"Add loan tapped");
    [self importLoanWithLoanNumber:((EncompassLoanInfo *)self.loanArray[sender.tag]).LoanNumber];
}

//_________________________________________________________________________
#pragma mark - TableView Delegate & DataSource
//_________________________________________________________________________

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.loanArray? [self.loanArray count] : 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommonItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"itemCell"];
    EncompassLoanInfo *loanInfo = self.loanArray[indexPath.row];
    cell.titleLabel.text =[NSString stringWithFormat:@"%@",loanInfo.BorrowerLastName];
    cell.detailsLabel.text=loanInfo.LoanNumber;
    cell.disclossureButton.tag = indexPath.row;
    [cell.disclossureButton setImage:[UIImage imageNamed:@"plus_icon"] forState:UIControlStateNormal];
    
    if (![cell.disclossureButton.allTargets containsObject:self]) {
        [cell.disclossureButton addTarget:self action:@selector(importSelectedLoan:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[self performSegueWithIdentifier:@"LoanDetailsSegue" sender:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//_________________________________________________________________________
#pragma mark - API
//_________________________________________________________________________

-(void) getData
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    __weak typeof(self) weakSelf = self;
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"LoanImportMobile" parameters:@{@"GetEncompassLoans":@"true"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get Encompass Loans : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            NSDictionary *result = responseObject;
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    strongSelf.loanArray=[EncompassLoanInfo arrayOfModelsFromDictionaries:responseObject[@"Results"][@"Loans"]];
                    
                    [SVProgressHUD dismiss];
                    [strongSelf.tableView reloadData];
                }
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error EncompassLoanInfo: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

-(void)importLoanWithLoanNumber:(NSString *)loanNumber
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    NSDictionary *param = @{
                            @"AddEncompassLoan":@"true",
                            @"LoanNumber" : loanNumber
                            };
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"LoanImportMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"LoanImportMobile Loans : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            NSDictionary *result = responseObject;
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Loan successfully pulled."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error EncompassLoanInfo: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

@end