//
//  AgentsVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "AgentsVC.h"

@interface AgentsVC (){
}

@end

@implementation AgentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([AFNetworkReachabilityManager sharedManager].reachable) {
        [self requestToAgentList];
    }
    else{
        NSLog(@"Offline");
        //[self setupOflineData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}
-(IBAction)inviteAgent:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invite an Agent" message:@"Enter their email address bellow." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Invite",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].keyboardType=UIKeyboardTypeEmailAddress;
    [alert show];

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        if ([InputValidator isEmailValid:[alertView textFieldAtIndex:0].text]) {
            [self requestToAddAgent:[alertView textFieldAtIndex:0].text];
        }
    }
}

#pragma mark - API Methods
-(void) requestToAgentList {
    __weak typeof(self) weakSelf = self;
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
     [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager] GET:@"AgentsMobile" parameters:@{@"Agents":@"true"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get Agents : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            NSDictionary *result = responseObject;
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    strongSelf.agentArray=[AgentInfo arrayOfModelsFromDictionaries:responseObject[@"Results"][@"Agents"]];
                   [SVProgressHUD dismiss];
                    
                    [strongSelf.tableView reloadData];
                }
            }
            else{
               [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void) requestToAddAgent:(NSString *)string{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    NSDictionary *param = @{
                            @"Add":@"true",
                            @"Email" :string
                            };
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"AgentsMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Agent Successfully Added."];
                [self requestToAgentList];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"agentDetailsSegue"]) {
        NSIndexPath *selectedIndexPath = sender;
        AgentInfo *agentInfo1 = self.agentArray[selectedIndexPath.row];
        AgentDetailsVC *vc = (AgentDetailsVC *)segue.destinationViewController;
        vc.agentInfo = agentInfo1;
    }
}
#pragma mark - TableView Delegate/DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.agentArray? [self.agentArray count] : 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AgentsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AgentCell"];
    AgentInfo *agentInfo = self.agentArray[indexPath.row];
   // cell.titleLabel.text =[NSString stringWithFormat:@"%@ %@",agentInfo.First_Name,agentInfo.Last_Name];
    cell.titleLabel.text=agentInfo.Display_Name;
    
    NSString *imgURL =[NSString stringWithFormat:@"%@",agentInfo.Photo_URL];
    [cell.iconView setImageWithURL:[NSURL URLWithString:imgURL]
            placeholderImage:[UIImage imageNamed:@"pic_holder"]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     [self performSegueWithIdentifier:@"agentDetailsSegue" sender:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.agentArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

@end

