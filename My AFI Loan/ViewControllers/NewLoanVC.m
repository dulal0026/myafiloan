//
//  NewLoanVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/30/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "NewLoanVC.h"

typedef NS_ENUM(NSInteger, HeaderType) {
    mandatoryFieldHeader,
    optionalFieldHeader
};
typedef NS_ENUM(NSInteger, SectionRowType) {
    commonInputRow,
    switchRow,
    commonInputGrayRow,
    DOBInputRow,
    commonActionRow
};
@interface NewLoanVC ()

@end

@implementation NewLoanVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    NSLog(@"%@", NSStringFromSelector(_cmd));
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonInputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonActionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonActionCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonInputSwitchCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonInputSwitchCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"DOBInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DOBInputCell"];
  
    // Mandatory Fields Section ------------------------------------------------------
    AKDTableSection *mandatoryFieldSection = [AKDTableSection new];
    mandatoryFieldSection.typeID = mandatoryFieldHeader;
    mandatoryFieldSection.sectionInfo = @"Duration";
    mandatoryFieldSection.sectionHeaderHeight = 0;
    mandatoryFieldSection.isCollapsed=NO;
    NSMutableArray *rowArray=[NSMutableArray new];

    DLTableRow *dlTableRow=[DLTableRow new];
    dlTableRow.typeID=commonInputRow;
    dlTableRow.rowTitle=@"First Name";
    dlTableRow.rowValue=@"";
    [rowArray addObject:dlTableRow];
    
    DLTableRow *dlTableRow1=[DLTableRow new];
    dlTableRow1.typeID=commonInputRow;
    dlTableRow1.rowTitle=@"Last Name";
    dlTableRow1.rowValue=@"";
    [rowArray addObject:dlTableRow1];
    
    DLTableRow *dlTableRow2=[DLTableRow new];
    dlTableRow2.typeID=commonInputRow;
    dlTableRow2.rowTitle=@"Email";
    dlTableRow2.rowValue=@"";
    [rowArray addObject:dlTableRow2];
    
    DLTableRow *dlTableRow3=[DLTableRow new];
    dlTableRow3.typeID=switchRow;
    dlTableRow3.rowTitle=@"Encompass Sync";
    dlTableRow3.boolValue=NO;
    [rowArray addObject:dlTableRow3];
    
    mandatoryFieldSection.rowsData = rowArray;
    
    // Optional Fields Section ------------------------------------------------------
    AKDTableSection *optionalFieldSection = [AKDTableSection new];
    optionalFieldSection.typeID = optionalFieldHeader;
    optionalFieldSection.isCollapsed=YES;
    optionalFieldSection.sectionInfo = @"Optional Fields";
    optionalFieldSection.sectionHeaderHeight = 44;
     NSMutableArray *rowArray2=[NSMutableArray new];
    
    DLTableRow *dlTableRow20=[DLTableRow new];
    dlTableRow20.typeID=commonInputRow;
    dlTableRow20.rowTitle=@"SSN";
    dlTableRow20.rowValue=@"";
    [rowArray2 addObject:dlTableRow20];
    
    DLTableRow *dlTableRow21=[DLTableRow new];
    dlTableRow21.typeID=DOBInputRow;
    dlTableRow21.rowTitle=@"DOB";
    dlTableRow21.rowValue=@" ";
    [rowArray2 addObject:dlTableRow21];
    
    DLTableRow *dlTableRow22=[DLTableRow new];
    dlTableRow22.typeID=commonInputRow;
    dlTableRow22.rowTitle=@"Home Phone";
    dlTableRow22.rowValue=@"";
    [rowArray2 addObject:dlTableRow22];
    
    DLTableRow *dlTableRow23=[DLTableRow new];
    dlTableRow23.typeID=commonInputRow;
    dlTableRow23.rowTitle=@"Cell Phone";
    dlTableRow23.rowValue=@"";
    [rowArray2 addObject:dlTableRow23];
    
    DLTableRow *dlTableRow24=[DLTableRow new];
    dlTableRow24.typeID=commonInputRow;
    dlTableRow24.rowTitle=@"Work Phone";
    dlTableRow24.rowValue=@"";
    [rowArray2 addObject:dlTableRow24];

    optionalFieldSection.rowsData =rowArray2;
    
    // Create Loan  Section ------------------------------------------------------
    AKDTableSection *createLoanSection = [AKDTableSection new];
    createLoanSection.typeID = optionalFieldHeader;
    createLoanSection.isCollapsed=NO;
    createLoanSection.sectionInfo = @"";
    createLoanSection.sectionHeaderHeight = 0;
    
    NSMutableArray *rowArray3=[NSMutableArray new];
    
    DLTableRow *dlTableRow30=[DLTableRow new];
    dlTableRow30.typeID=commonActionRow;
    dlTableRow30.rowTitle=@"CREATE";
    [rowArray3 addObject:dlTableRow30];
    
    createLoanSection.rowsData =rowArray3;

    self.tableData = [@[
                        mandatoryFieldSection,
                        optionalFieldSection,
                        createLoanSection
                        ] mutableCopy];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
//------------------------------------------------------
#pragma mark - TableView Delegate/DataSource
//------------------------------------------------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.tableData count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.isCollapsed? 0 : [sectionObj.rowsData count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.sectionHeaderHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    AKDTableSection *sectionObj = self.tableData[section];
    
    if (sectionObj.typeID == mandatoryFieldHeader) {
        EmptyHeaderView *dView = [EmptyHeaderView instanceWithNibName:nil bundle:nil owner:nil];
        if (dView) {
            //            dView
        }
        headerView = dView;
    }
    else if (sectionObj.typeID == optionalFieldHeader) {
        OptionalFieldHeader *header = [OptionalFieldHeader instanceWithNibName:nil bundle:nil owner:nil];
        if (header) {
            //            dView
            header.backView.layer.shadowOffset = CGSizeMake(5, 0);
           // header.titleLabel.text=sectionObj.sectionInfo;
            header.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            header.backView.layer.shadowOpacity = 1;
            header.backView.layer.shadowRadius = 1.0;
            header.collapseButton.tag=section;
            header.titleLabel.text=(NSString*)sectionObj.sectionInfo;
            
            if (sectionObj.isCollapsed)[header.collapseButton setImage:expandCloseImage forState:UIControlStateNormal];
            else [header.collapseButton setImage:expandImage forState:UIControlStateNormal];
            [header.collapseButton addTarget:self action:@selector(collapseButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        headerView = header;
    }
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
    static NSString *identifier;
    if (dLTableRow.typeID==commonInputRow) {
        identifier = @"CommonInputCell";
        CommonInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
         cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=dLTableRow.rowValue;
   
        if (sectionObj.typeID==optionalFieldHeader) cell.bgView.backgroundColor=[UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1.0];
        
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        
        if (![cell.inputField.allTargets containsObject:self]) {
            [cell.inputField addTarget:self action:@selector(textFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
        }
        
        return cell;
    }
    else if (dLTableRow.typeID==switchRow) {
        identifier = @"CommonInputSwitchCell";
        CommonInputSwitchCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.syncSwitch.on=dLTableRow.boolValue;
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        
        if (![cell.syncSwitch.allTargets containsObject:self]) {
            [cell.syncSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        }
        return cell;
    }
    else if (dLTableRow.typeID==DOBInputRow) {
        identifier = @"DOBInputCell";
        DOBInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.valueLabel.text=dLTableRow.rowValue;
        
        if (sectionObj.typeID==optionalFieldHeader) cell.bgView.backgroundColor=[UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1.0];
        
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.valueLabel.userInteractionEnabled=YES;
        
        UITapGestureRecognizer *dOBLabelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(datePickerLabelClicked:)];
        [cell.valueLabel addGestureRecognizer:dOBLabelTap];
        return cell;
    }
    else{
        
        identifier = @"CommonActionCell";
        CommonActionCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        [cell.actionButton setTitle:dLTableRow.rowTitle forState:UIControlStateNormal];
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 5);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        [cell.actionButton addTarget:self action:@selector(createLoan:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}
-(void)collapseButtonTapAction:(id)sender{
    UIButton *button=(UIButton*)sender;
    [self tableView:self.tableView toggleSection:button.tag];
}
-(void)createLoan:(id)sender{
      AKDTableSection *mandatorySectionObj = self.tableData[0];
    AKDTableSection *optionalSectionObj = self.tableData[1];

    if ([InputValidator validateCreateLoanForm:((DLTableRow *)mandatorySectionObj.rowsData[0]).rowValue lastName:((DLTableRow *)mandatorySectionObj.rowsData[1]).rowValue email:((DLTableRow *)mandatorySectionObj.rowsData[2]).rowValue]) {
        if ([AFNetworkReachabilityManager sharedManager].reachable){
            [self requestToCreateLoan:mandatorySectionObj optionalSection:optionalSectionObj];
        }
        else [AlertManager showAlertSingle:@"Connection Error" msg:noInternetAlertMsg];
    }
}
//----------------------------------------------------------------
#pragma mark - API -
#pragma mark Password Reset
//----------------------------------------------------------------
-(void)requestToCreateLoan:(AKDTableSection *)mandatorySection optionalSection:(AKDTableSection*)optionalSection
{
    NSString *encompassSyncValue=((DLTableRow *)mandatorySection.rowsData[3]).boolValue?@"True":@"False";
    NSDictionary *param = @{
                            @"NewLoan":@"true",
                            @"Buyer_First" : ((DLTableRow *)mandatorySection.rowsData[0]).rowValue,
                            @"Buyer_Last" : ((DLTableRow *)mandatorySection.rowsData[1]).rowValue,
                            @"Buyer_Email" : ((DLTableRow *)mandatorySection.rowsData[2]).rowValue,
                            @"Encompass_Sync" : encompassSyncValue,
                            @"Buyer_Work_Phone" : ((DLTableRow *)optionalSection.rowsData[4]).rowValue,
                            @"Buyer_Cell_Phone" : ((DLTableRow *)optionalSection.rowsData[3]).rowValue,
                            @"Buyer_Home_Phone" : ((DLTableRow *)optionalSection.rowsData[2]).rowValue,
                            @"Buyer_DOB" : ((DLTableRow *)optionalSection.rowsData[1]).rowValue,
                            @"Buyer_SSN" : ((DLTableRow *)optionalSection.rowsData[0]).rowValue
                            };
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"CreateLoanMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Reset Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Loan Created Successfully."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error creating new loan: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

//------------------------------------------------------
#pragma mark - TableView Utils (Private)
//------------------------------------------------------

-(void) tableView:(UITableView *) tableView toggleSection:(NSInteger) section {
    AKDTableSection *sectionObj = self.tableData[section];
    sectionObj.isCollapsed = !sectionObj.isCollapsed;
    
    [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UITextField Value change Inspector
-(void) textFieldTextChanged:(UITextField *)sender
{
    if ([sender firstSuperviewOfClass:[CommonInputCell class]])
    {
        CommonInputCell *cell = (CommonInputCell *)[sender firstSuperviewOfClass:[CommonInputCell class]];
        if (cell) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            AKDTableSection *sectionObj = self.tableData[indexPath.section];
            DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
            dLTableRow.rowValue = sender.text;
        }
    }
}
-(void) switchValueChanged:(UISwitch *)sender{
    if ([sender firstSuperviewOfClass:[CommonInputSwitchCell class]])
    {
        CommonInputSwitchCell *cell = (CommonInputSwitchCell *)[sender firstSuperviewOfClass:[CommonInputSwitchCell class]];
        if (cell) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            AKDTableSection *sectionObj = self.tableData[indexPath.section];
            DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
            dLTableRow.boolValue =sender.on?YES:NO;
        }
    }
}

//------------------------------------------------------
#pragma mark - UIDate Picker Utils (Private)
//------------------------------------------------------

- (void)datePickerLabelClicked:(id)sender
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select DOB" delegate:self];
    //[picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}
-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateStyle:NSDateFormatterMediumStyle];
   // [formatter setTimeStyle:NSDateFormatterNoStyle];
    [formatter setDateFormat:@"MM-dd-YYYY"];
    AKDTableSection *sectionObj = self.tableData[1];
    DLTableRow *dLTableRow = sectionObj.rowsData[1];
    dLTableRow.rowValue=[formatter stringFromDate:date];
    [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
}
@end
