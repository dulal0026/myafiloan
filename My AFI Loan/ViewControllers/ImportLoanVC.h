//
//  ImportLoanVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/29/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "CommonItemCell.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"
#import "ApplicationTextManager.h"
#import "EncompassLoanInfo.h"
#import "AlertManager.h"

@interface ImportLoanVC : UIViewController{
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *loanArray;
@property(nonatomic,strong)NSString *sessionId;
@property(nonatomic,strong)NSString *valueOfSessionID;

- (IBAction)showMenu:(id)sender;
-(IBAction)goBack:(id)sender;
@end

