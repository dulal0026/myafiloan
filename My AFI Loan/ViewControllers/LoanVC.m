//
//  ViewController.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "LoanVC.h"
#import "UIViewController+MMDrawerController.h"

@interface LoanVC (){
}
@end

@implementation LoanVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSInteger userType=[[AppUserDefault sharedInstance] getIntegerValueforKey:userTypeKey];
    if (userType==1) _addLoanButton.hidden=NO;
    else _addLoanButton.hidden=YES;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonItemCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"itemCell"];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if ([AFNetworkReachabilityManager sharedManager].reachable) {
        NSLog(@"Online");
        [self getData];
    }
    else{
        
        NSLog(@"Offline");
        //[self setupOflineData];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}

-(IBAction)addLoanButtonTapAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Would you like to Import a loan Encompass, or Create a new loan?" message:nil delegate:self cancelButtonTitle:@"Import" otherButtonTitles:@"New",nil];
    [alert show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0)[self performSegueWithIdentifier:@"ImportSegue" sender:self];
    else [self performSegueWithIdentifier:@"NewLoanSegue" sender:self];
}

-(void) getData {
    __weak typeof(self) weakSelf = self;
    [SVProgressHUD showWithStatus:@"Please wait..."];
      [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];

     [[AFILoanRequestManager manager] GET:@"LoansMobile" parameters:@{@"Loans":@"true"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"Get Loans : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                strongSelf.loanArray=[LoanInfo arrayOfModelsFromDictionaries:responseObject[@"Results"][@"Loans"]];
                [SVProgressHUD dismiss];
                [strongSelf.tableView reloadData];
            }
        }
         
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting Loan info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

#pragma mark - TableView Delegate/DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.loanArray? [self.loanArray count] : 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommonItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"itemCell"];
    LoanInfo *loanInfo = self.loanArray[indexPath.row];
    cell.titleLabel.text =[NSString stringWithFormat:@"%@ %@",loanInfo.Borrower_First_Name,loanInfo.Borrower_Last_Name];
    cell.detailsLabel.text=loanInfo.Loan_Number;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[AppUserDefault sharedInstance] getIntegerValueforKey:userTypeKey]==1?[self performSegueWithIdentifier:@"LoanDetailsSegue" sender:indexPath]:[self performSegueWithIdentifier:@"LoanDetailsBBASegue" sender:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
    if ([segue.identifier isEqualToString:@"LoanDetailsSegue"]) {
        NSIndexPath *selectedIndexPath = sender;
        LoanInfo *loanInfo = self.loanArray[selectedIndexPath.row];
        LoanDetailsVC *vc = (LoanDetailsVC *)segue.destinationViewController;
        vc.loanNo = loanInfo.Loan_Number;
    }
    if ([segue.identifier isEqualToString:@"LoanDetailsBBASegue"]) {
        NSIndexPath *selectedIndexPath = sender;
        LoanInfo *loanInfo = self.loanArray[selectedIndexPath.row];
        LoanDetailsBBAVC *vc = (LoanDetailsBBAVC *)segue.destinationViewController;
        vc.loanNo = loanInfo.Loan_Number;
    }
}
@end
