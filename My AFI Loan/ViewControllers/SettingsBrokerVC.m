//
//  SettingsBrokerVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "SettingsBrokerVC.h"
#import "SVProgressHUD.h"
#import "ApplicationTextManager.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"
#import "AlertManager.h"
#import "InputValidator.h"

typedef NS_ENUM(NSInteger, SectionRowType) {
    CommonInputRow,
    CommonActionRow,
    EmailInputRow
};
@interface SettingsBrokerVC ()
@property (strong, nonatomic) PhotoHeaderView *header;
@end

@implementation SettingsBrokerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonInputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonActionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonActionCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EmailInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"EmailInputCell"];
    [self getBrokerSettingData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}
-(void)uploadPhotoButtonAction:(id)sender
{
    NSLog(@"uploadPhotoButtonAction");
    UIActionSheet *aSheet = [[UIActionSheet alloc] initWithTitle:@"Select option:"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:@"Use Camera",@"Choose From Camera-Roll", nil];
    [aSheet showInView:self.view];
}

-(void)saveButtonTapAction:(id)sender
{
    UIButton *button=(UIButton*)sender;
    AKDTableSection *sectionObj = self.tableData[button.tag];
    
    switch (button.tag)
    {
        case 0:
        {
            NSLog(@"information Save Pressed");
            if ([InputValidator isBrokerInfoValid:((DLTableRow *)sectionObj.rowsData[0]).rowValue address:((DLTableRow *)sectionObj.rowsData[1]).rowValue phone:((DLTableRow *)sectionObj.rowsData[2]).rowValue]) {
                
                [self requestToChangeAccountInfo:sectionObj];
            }
                
            break;
        }
            
        case 1:
        {
            NSLog(@"Submit button Pressed");
            if ([InputValidator validateEmail:((DLTableRow *)sectionObj.rowsData[0]).rowValue]) {
                [self requestToAddEmail:button];
              
            } else
                [AlertManager showAlertSingle:@"Invalid Email!!" msg:@"Please enter valid email address."];
            
            break;
        }
            
        case 2:
        {
            NSLog(@"Password Save Pressed");
            if ([InputValidator isPasswordFormValid:((DLTableRow *)sectionObj.rowsData[0]).rowValue
                                            newpass:((DLTableRow *)sectionObj.rowsData[1]).rowValue
                                             rePass:((DLTableRow *)sectionObj.rowsData[2]).rowValue]) {
                
                [self requestToChangePassword:sectionObj];
            }
            
            break;
        }
            
        default:
            break;
    }
}

//------------------------------------------------------
#pragma mark - TableView Delegate/DataSource
//------------------------------------------------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.tableData count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.isCollapsed? 0 : [sectionObj.rowsData count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.sectionHeaderHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    AKDTableSection *sectionObj = self.tableData[section];
    
    if (sectionObj.typeID == 0) {
        self.header = [PhotoHeaderView instanceWithNibName:nil bundle:nil owner:nil];
        if(self.header){
            self.header.headerView.layer.shadowOffset = CGSizeMake(5, 5);
            self.header.headerView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            self.header.headerView.layer.shadowOpacity = 1;
            self.header.headerView.layer.shadowRadius = 1.0;
            
            NSString *imgURL =[NSString stringWithFormat:@"%@",sectionObj.pictureUrl];
            NSString* encodedImageUrl = [imgURL stringByAddingPercentEscapesUsingEncoding:
                                         NSUTF8StringEncoding];
        
            [self.header.imageView sd_setImageWithURL:[NSURL URLWithString:encodedImageUrl]
                       placeholderImage:[UIImage imageNamed:@"pic_holder"]];
            
            [self.header.addPhotoButton addTarget:self action:@selector(uploadPhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnHeader:)];
            [self.header addGestureRecognizer:tapGesture];
        }
        headerView = self.header;
    }
    else if (sectionObj.typeID == 1) {
        AccountHeader *header = [AccountHeader instanceWithNibName:nil bundle:nil owner:nil];
        if (header) {
            //            dView
            header.backView.layer.shadowOffset = CGSizeMake(5, 0);
            header.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            header.backView.layer.shadowOpacity = 1;
            header.backView.layer.shadowRadius = 1.0;
            header.collapseButton.tag=section;
            [header.collapseButton addTarget:self action:@selector(collapseButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        headerView = header;
    }
    else if (sectionObj.typeID == 2) {
        EmptyHeaderView *dView = [EmptyHeaderView instanceWithNibName:nil bundle:nil owner:nil];
        if (dView) {
            //            dView
        }
        headerView = dView;
    }
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
    static NSString *identifier;
    if (dLTableRow.typeID==EmailInputRow){
        identifier = @"EmailInputCell";
        EmailInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        //cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        // cell.bgView.layer.shadowOffset = CGSizeMake(5, 5);
        // cell.bgView.layer.shadowOpacity = 1;
        //cell.bgView.layer.shadowRadius = 1.0;
        cell.inputField.text = dLTableRow.rowValue;
        cell.inputField.keyboardType=UIKeyboardTypeEmailAddress;
        cell.actionButton.tag=indexPath.section;
        
        if (![cell.inputField.allTargets containsObject:self]) {
            [cell.inputField addTarget:self action:@selector(textFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
        }
        
        if (![cell.actionButton.allTargets containsObject:self]) {
            [cell.actionButton addTarget:self action:@selector(saveButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        return cell;
    }
    if (dLTableRow.typeID==CommonActionRow)
    {
        identifier = @"CommonActionCell";
        CommonActionCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 5);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.actionButton.tag=indexPath.section;
        
        if (![cell.actionButton.allTargets containsObject:self]) {
            [cell.actionButton addTarget:self action:@selector(saveButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        return cell;
    }
    else
    {
        identifier = @"CommonInputCell";
        CommonInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=dLTableRow.rowValue;
        if (indexPath.section==1)
            cell.inputField.hidden=YES;
        
        if (![cell.inputField.allTargets containsObject:self]) {
            [cell.inputField addTarget:self action:@selector(textFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
        }
        
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        return cell;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    
    if (sectionObj.typeID == 0) { // Massage for
        NSMutableDictionary *info = [sectionObj.sectionInfo mutableCopy];
        info[@"title"] = sectionObj.rowsData[indexPath.row];
        sectionObj.sectionInfo = [info copy];
        
        if(indexPath.section==0){
            //[self updateSectionsByMassageForIndex:indexPath.row];
        }
    }
    
    [self tableView:tableView toggleSection:indexPath.section];*/
}
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL flag=NO;
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section==1 && indexPath.row!=0) {
        flag=YES;
    }
    return flag;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSLog(@"Delete done");
       // [self.agentArray removeObjectAtIndex:indexPath.row];
        
        [self requestToRemoveEmail:indexPath];
       
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

//------------------------------------------------------
#pragma mark - TableView Utils (Private)
//------------------------------------------------------
-(void) tableView:(UITableView *) tableView reloadSection:(NSInteger) section {
    
    [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void) tableView:(UITableView *) tableView toggleSection:(NSInteger) section {
    AKDTableSection *sectionObj = self.tableData[section];
    sectionObj.isCollapsed = !sectionObj.isCollapsed;
    
    [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void) tappedOnHeader:(UIGestureRecognizer *) gesture {
    UIView *header = gesture.view;
    if ([header isKindOfClass:[PhotoHeaderView class]]) {
        //[self tableView:self.tableView toggleSection:((PhotoHeaderView *) header).section];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 95;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}
-(void)collapseButtonTapAction:(id)sender{
    UIButton *button=(UIButton*)sender;
    
    [self requestToGetEmails:button];
}

//------------------------------------------------------
#pragma mark - API -
#pragma mark - Api methods
//------------------------------------------------------
-(void) getBrokerSettingData {
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsBrokerMobile" parameters:@{@"Settings":@"true"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get SettingsBrokerMobile : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                NSDictionary *infoDictionary=[responseObject objectForKey:@"Results"];
                AKDTableSection *sectionInformation = [AKDTableSection new];
                sectionInformation.typeID = 0;
                
                sectionInformation.isCollapsed = NO;
                sectionInformation.sectionHeaderHeight = 95;
                sectionInformation.sectionInfo = infoDictionary;
                sectionInformation.pictureUrl=[infoDictionary valueForKey:@"PicturePath"];
                
                NSMutableArray *infoFieldArray = [@[ @"Brokerage",@"Address",@"Phone",@""] mutableCopy];
                
                
                NSMutableArray *rowArray=[NSMutableArray new];
                DLTableRow *dlTableRow;
                for (int index=0; index<[infoFieldArray count]; index++) {
                    if ([[infoFieldArray objectAtIndex:index] isEqualToString:@""]) {
                        dlTableRow=[DLTableRow new];
                        dlTableRow.typeID=CommonActionRow;
                        [rowArray addObject:dlTableRow];
                    }
                    else{
                        dlTableRow=[DLTableRow new];
                        dlTableRow.typeID=CommonInputRow;
                        dlTableRow.rowTitle=[infoFieldArray  objectAtIndex:index];
                        dlTableRow.rowValue=[UtilityManager findOutValue:[infoFieldArray objectAtIndex:index] source:infoDictionary];
                        
                        [rowArray addObject:dlTableRow];
                    }
                }
                
                sectionInformation.rowsData=rowArray;
                
                // Email ------------------------------------------------------
                AKDTableSection *sectionEmail = [AKDTableSection new];
                sectionEmail.typeID = 1;
                sectionEmail.sectionHeaderHeight = 44;
                sectionEmail.isCollapsed=YES;
                
                // Password ------------------------------------------------------
                
                AKDTableSection *sectionPassword = [AKDTableSection new];
                sectionPassword.typeID = 2;
                sectionPassword.sectionHeaderHeight = 10;
                
                NSMutableArray *rowArray2=[NSMutableArray new];
                DLTableRow *dlTableRow20=[DLTableRow new];
                dlTableRow20.typeID=CommonInputRow;
                dlTableRow20.rowTitle=@"Old Password";
                dlTableRow20.rowValue=@"";
                [rowArray2 addObject:dlTableRow20];
                
                DLTableRow *dlTableRow21=[DLTableRow new];
                dlTableRow21.typeID=CommonInputRow;
                dlTableRow21.rowTitle=@"New Password";
                dlTableRow21.rowValue=@"";
                [rowArray2 addObject:dlTableRow21];
                
                DLTableRow *dlTableRow22=[DLTableRow new];
                dlTableRow22.typeID=CommonInputRow;
                dlTableRow22.rowTitle=@"Repeat Password";
                dlTableRow22.rowValue=@"";
                [rowArray2 addObject:dlTableRow22];
                
                DLTableRow *dlTableRow23=[DLTableRow new];
                dlTableRow23.typeID=CommonActionRow;
                dlTableRow23.rowTitle=@"";
                dlTableRow23.rowValue=@"";
                [rowArray2 addObject:dlTableRow23];
                
                sectionPassword.rowsData =rowArray2;
                
                self.tableData = [@[
                                    sectionInformation,
                                    sectionEmail,
                                    sectionPassword
                                    ] mutableCopy];
                [self.tableView reloadData];
                
                self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[responseObject objectForKey:@"General"] valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

-(void)requestToChangeAccountInfo:(AKDTableSection *)section
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    NSDictionary *param = @{
                            @"UpdateAccountInfo":@"true",
                            @"CompanyName" : ((DLTableRow *)section.rowsData[0]).rowValue,
                            @"Address" : ((DLTableRow *)section.rowsData[1]).rowValue,
                            @"PhoneNumber" : ((DLTableRow *)section.rowsData[2]).rowValue
                            };
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsBrokerMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Information Successfully updated."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void)requestToChangePassword:(AKDTableSection *)section
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    NSDictionary *param = @{
                            @"UpdatePassword":@"true",
                            @"CurrentPassword" : ((DLTableRow *)section.rowsData[0]).rowValue,
                            @"NewPassword" : ((DLTableRow *)section.rowsData[1]).rowValue
                            };
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsBrokerMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Password Successfully updated."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void) requestToGetEmails:(UIButton*)sender
{
    AKDTableSection *sectionObj = self.tableData[sender.tag];
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    NSDictionary *param = @{ @"Emails":@"true"};
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsBrokerMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Getting Emails : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD dismiss];
                
                NSDictionary *infoDictionary=[responseObject objectForKey:@"Results"];
                NSMutableArray *emailArray=[infoDictionary valueForKey:@"Emails"];
                NSLog(@"emils:%@",emailArray);
                
                NSMutableArray *rowArray=[NSMutableArray new];
                DLTableRow *dlTableRow;
                for (int index=0; index<[emailArray count]+1; index++) {
                    if (index==0) {
                        dlTableRow=[DLTableRow new];
                        dlTableRow.typeID=EmailInputRow;
                        dlTableRow.rowTitle=@"";
                        dlTableRow.rowValue=@"";
                        [rowArray addObject:dlTableRow];
                    }
                    else{
                        dlTableRow=[DLTableRow new];
                        dlTableRow.typeID=CommonInputRow;
                        dlTableRow.rowTitle=[emailArray  objectAtIndex:index-1];
                        dlTableRow.rowValue=[emailArray  objectAtIndex:index-1];
                        [rowArray addObject:dlTableRow];
                    }
                }
                sectionObj.rowsData=rowArray;
                
                [self tableView:self.tableView toggleSection:sender.tag];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

-(void) requestToAddEmail:(UIButton *)sender{
    AKDTableSection *section = self.tableData[sender.tag];
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    NSDictionary *param = @{
                            @"AddEmail":@"true",
                            @"Email" : ((DLTableRow *)section.rowsData[0]).rowValue
                            };
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsBrokerMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Email Successfully Added."];
                
                ((DLTableRow *)section.rowsData[0]).rowTitle=((DLTableRow *)section.rowsData[0]).rowValue;
                
                DLTableRow *dlTableRow0=[DLTableRow new];
                dlTableRow0.typeID=CommonInputRow;
                dlTableRow0.rowTitle= ((DLTableRow *)section.rowsData[0]).rowTitle;
                dlTableRow0.rowValue= ((DLTableRow *)section.rowsData[0]).rowValue;
                [section.rowsData addObject:dlTableRow0];
                
                DLTableRow *dlTableRow=[DLTableRow new];
                dlTableRow.typeID=EmailInputRow;
                dlTableRow.rowTitle=@"";
                dlTableRow.rowValue=@"";
               
                [section.rowsData replaceObjectAtIndex:0 withObject:dlTableRow];
                [self tableView:self.tableView reloadSection:sender.tag];
                
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

-(void) requestToRemoveEmail:(NSIndexPath*)indexpath{
    NSInteger sectionIndex=indexpath.section;
    NSInteger rowIndex=indexpath.row;
    AKDTableSection *section = self.tableData[sectionIndex];
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    NSDictionary *param = @{
                            @"RemoveEmail":@"true",
                            @"Email" : ((DLTableRow *)section.rowsData[rowIndex-1]).rowValue
                            };
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsBrokerMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Email Successfully Removed."];
                [section.rowsData removeObjectAtIndex:rowIndex-1];
                [self.tableView deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationFade];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

#pragma mark - uploadPhoto
-(void)uploadPhoto:(UIImage *)image
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    AFHTTPRequestOperation *op = [[AFILoanRequestManager manager] POST:@"SettingsBrokerMobile" parameters:@{@"UpdatePhoto":@"true"} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData
                                    name:@"photo"
                                fileName:@"photo.jpeg"
                                mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *JSON = (NSDictionary *)responseObject;
        NSLog(@"responseObject = %@",JSON);
        [SVProgressHUD showSuccessWithStatus:@"Photo Successfully uploaded."];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
    
    [op setUploadProgressBlock:^(NSUInteger bytesWritten,
                                 long long totalBytesWritten,
                                 long long totalBytesExpectedToWrite) {
        NSLog(@"Wrote %ld/%ld", (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
    }];
    
    [op start];
}

#pragma mark - UITextField Value change Inspector
-(void) textFieldTextChanged:(UITextField *)sender
{
    if ([sender firstSuperviewOfClass:[CommonInputCell class]])
    {
        CommonInputCell *cell = (CommonInputCell *)[sender firstSuperviewOfClass:[CommonInputCell class]];
        if (cell) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            AKDTableSection *sectionObj = self.tableData[indexPath.section];
            DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
            dLTableRow.rowValue = sender.text;
        }
    }
    else if ([sender firstSuperviewOfClass:[EmailInputCell class]])
    {
        EmailInputCell *cell = (EmailInputCell *)[sender firstSuperviewOfClass:[EmailInputCell class]];
        if (cell) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            AKDTableSection *sectionObj = self.tableData[indexPath.section];
            DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
            dLTableRow.rowValue = sender.text;
        }
    }
}

//=================================================================
#pragma mark - ActionSheet Delegates
//=================================================================
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self openWithSourceType:UIImagePickerControllerSourceTypeCamera];
            break;
        case 1:
            [self openWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        case 2:
            NSLog(@"Cancel.");
            break;
            
        default:
            break;
    }
}

-(void)openWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    pickerController.allowsEditing = NO;
    pickerController.sourceType = sourceType;
    pickerController.modalPresentationStyle = UIModalPresentationFullScreen;
    
    if (IS_IOS_8_OR_LATER) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self presentViewController:pickerController animated:YES completion:nil];
        }];
    }
    else
        [self presentViewController:pickerController animated:YES completion:nil];
}

//=================================================================
#pragma mark - ImagePicker Delegates
//=================================================================

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.header.imageView.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self uploadPhoto:chosenImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

@end























