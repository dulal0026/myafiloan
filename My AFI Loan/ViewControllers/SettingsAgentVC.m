//
//  SettingsAgentVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "SettingsAgentVC.h"
#import "SVProgressHUD.h"
#import "AlertManager.h"
#import "InputValidator.h"
typedef NS_ENUM(NSInteger, SectionRowType) {
    CommonInputRow,
    CommonActionRow
};
@interface SettingsAgentVC ()
@property (strong, nonatomic) PhotoHeaderView *header;
@end

@implementation SettingsAgentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonInputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonActionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonActionCell"];
    
    [self getAgentSettingData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}
-(void)uploadPhotoButtonAction:(id)sender
{
    NSLog(@"uploadPhotoButtonAction");
    UIActionSheet *aSheet = [[UIActionSheet alloc] initWithTitle:@"Select option:"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:@"Use Camera",@"Choose From Camera-Roll", nil];
    [aSheet showInView:self.view];
}

-(void)saveButtonTapAction:(id)sender
{
    UIButton *button=(UIButton*)sender;
    AKDTableSection *sectionObj = self.tableData[button.tag];
    
    switch (button.tag)
    {
        case 0:
        {
            NSLog(@"information Save Pressed");
            if ([InputValidator isAgentInfoValid:((DLTableRow *)sectionObj.rowsData[0]).rowValue
                                        lastName:((DLTableRow *)sectionObj.rowsData[1]).rowValue
                                      licenceNum:((DLTableRow *)sectionObj.rowsData[2]).rowValue
                                       brokerNum:((DLTableRow *)sectionObj.rowsData[3]).rowValue
                                         address:((DLTableRow *)sectionObj.rowsData[4]).rowValue
                                     officePhone:((DLTableRow *)sectionObj.rowsData[5]).rowValue
                                     mobilePhone:((DLTableRow *)sectionObj.rowsData[6]).rowValue
                             additionalRecipient:((DLTableRow *)sectionObj.rowsData[7]).rowValue]) {
                
                [self requestToChangeAccountInfo:sectionObj];
            }
            break;
        }
        case 1:
        {
            NSLog(@"information Save Pressed");
            if ([InputValidator isAgentFooterInfoValid:((DLTableRow *)sectionObj.rowsData[0]).rowValue
                                        website:((DLTableRow *)sectionObj.rowsData[1]).rowValue
                                      facebook:((DLTableRow *)sectionObj.rowsData[2]).rowValue
                                       twitter:((DLTableRow *)sectionObj.rowsData[3]).rowValue
                                         linkedin:((DLTableRow *)sectionObj.rowsData[4]).rowValue]) {
                
                [self requestToChangeAccountFooterInfo:sectionObj];
            }
            break;
        }
            
        case 2:
        {
            NSLog(@"Password Save Pressed");
            if ([InputValidator isPasswordFormValid:((DLTableRow *)sectionObj.rowsData[0]).rowValue
                                            newpass:((DLTableRow *)sectionObj.rowsData[1]).rowValue
                                             rePass:((DLTableRow *)sectionObj.rowsData[2]).rowValue]) {
                
                [self requestToChangePassword:sectionObj];
            }
            
            break;
        }
            
        default:
            break;
    }
}

//------------------------------------------------------
#pragma mark - Api method
//------------------------------------------------------
-(void) getAgentSettingData {
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsAgentMobile" parameters:@{@"Settings":@"true"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get Loans : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                
                NSDictionary *infoDictionary=[responseObject objectForKey:@"Results"];
                
                AKDTableSection *sectionInformation = [AKDTableSection new];
                sectionInformation.typeID = 0;
                
                sectionInformation.isCollapsed = NO;
                sectionInformation.sectionHeaderHeight = 95;
                sectionInformation.sectionInfo = infoDictionary;
                sectionInformation.pictureUrl=[infoDictionary valueForKey:@"PicturePath"];
                
                 NSMutableArray *infoFieldArray = [@[ @"First",@"Last",@"LicenseNumber",@"BrokerNumber",@"Address",@"Office",@"Mobile",@"CCEmail",@"" ] mutableCopy];
                
                NSMutableArray *rowArray=[NSMutableArray new];
                DLTableRow *dlTableRow;
                for (int index=0; index<[infoFieldArray count]; index++) {
                    if ([[infoFieldArray objectAtIndex:index] isEqualToString:@""]) {
                        dlTableRow=[DLTableRow new];
                        dlTableRow.typeID=CommonActionRow;
                        [rowArray addObject:dlTableRow];
                    }
                    else{
                        dlTableRow=[DLTableRow new];
                        dlTableRow.typeID=CommonInputRow;
                        dlTableRow.rowTitle=[infoFieldArray  objectAtIndex:index];
                        dlTableRow.rowValue=[UtilityManager findOutValue:[infoFieldArray objectAtIndex:index] source:infoDictionary];
                        [rowArray addObject:dlTableRow];
                    }
                }
                
                sectionInformation.rowsData=rowArray;

                // Footer Info ------------------------------------------------------
                
                AKDTableSection *sectionFooter = [AKDTableSection new];
                sectionFooter.typeID = 1;
                sectionFooter.isCollapsed = NO;
                sectionFooter.sectionHeaderHeight = 0;
                sectionFooter.sectionInfo = @"";
                
                NSMutableArray *footerInfoFieldArray = [@[@"Company",@"Website",@"Facebook",@"Twitter",@"LinkedIn",@"" ] mutableCopy];
                
                NSMutableArray *footerRowArray=[NSMutableArray new];
                DLTableRow *dlTableRow00;
                for (int index=0; index<[footerInfoFieldArray count]; index++) {
                    if ([[footerInfoFieldArray objectAtIndex:index] isEqualToString:@""]) {
                        dlTableRow00=[DLTableRow new];
                        dlTableRow00.typeID=CommonActionRow;
                        [footerRowArray addObject:dlTableRow00];
                    }
                    else{
                        dlTableRow00=[DLTableRow new];
                        dlTableRow00.typeID=CommonInputRow;
                        dlTableRow00.rowTitle=[footerInfoFieldArray  objectAtIndex:index];
                        dlTableRow00.rowValue=[UtilityManager findOutValue:[footerInfoFieldArray objectAtIndex:index] source:infoDictionary];
                        [footerRowArray addObject:dlTableRow00];
                    }
                }
                sectionFooter.rowsData=footerRowArray;
                
                // Duration ------------------------------------------------------
                
                AKDTableSection *sectionPassword = [AKDTableSection new];
                sectionPassword.typeID = 1;
               
                sectionPassword.sectionHeaderHeight = 10;
                
                NSMutableArray *rowArray1=[NSMutableArray new];
                DLTableRow *dlTableRow10=[DLTableRow new];
                dlTableRow10.typeID=CommonInputRow;
                dlTableRow10.rowTitle=@"Old Password";
                dlTableRow10.rowValue=@"";
               
                [rowArray1 addObject:dlTableRow10];
                
               
                DLTableRow *dlTableRow11=[DLTableRow new];
                dlTableRow11.typeID=CommonInputRow;
                dlTableRow11.rowTitle=@"New Password";
                dlTableRow11.rowValue=@"";
               
                [rowArray1 addObject:dlTableRow11];
                
                DLTableRow *dlTableRow12=[DLTableRow new];
                dlTableRow12.typeID=CommonInputRow;
                dlTableRow12.rowTitle=@"Repeat Password";
                dlTableRow12.rowValue=@"";
          
                [rowArray1 addObject:dlTableRow12];
                
                DLTableRow *dlTableRow13=[DLTableRow new];
                dlTableRow13.typeID=CommonActionRow;
                dlTableRow13.rowTitle=@"";
                dlTableRow13.rowValue=@"";
                [rowArray1 addObject:dlTableRow13];
                
                sectionPassword.rowsData =rowArray1;
           
                self.tableData = [@[
                                    sectionInformation,
                                    sectionFooter,
                                    sectionPassword
                                    ] mutableCopy];
                [self.tableView reloadData];
                
                self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[responseObject objectForKey:@"General"] valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
          [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}


//------------------------------------------------------
#pragma mark - TableView Delegate/DataSource
//------------------------------------------------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.tableData count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.isCollapsed? 0 : [sectionObj.rowsData count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.sectionHeaderHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    AKDTableSection *sectionObj = self.tableData[section];
    
    if (sectionObj.typeID == 0) {
        self.header = [PhotoHeaderView instanceWithNibName:nil bundle:nil owner:nil];
        
        if(self.header){
            self.header.headerView.layer.shadowOffset = CGSizeMake(5, 5);
            self.header.headerView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            self.header.headerView.layer.shadowOpacity = 1;
            self.header.headerView.layer.shadowRadius = 1.0;
            
            NSString *imgURL =[NSString stringWithFormat:@"%@",sectionObj.pictureUrl];
            NSString* encodedImageUrl = [imgURL stringByAddingPercentEscapesUsingEncoding:
                                         NSUTF8StringEncoding];
            
            [self.header.imageView sd_setImageWithURL:[NSURL URLWithString:encodedImageUrl]
                                     placeholderImage:[UIImage imageNamed:@"pic_holder"]];

            [self.header.addPhotoButton addTarget:self action:@selector(uploadPhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnHeader:)];
            [self.header addGestureRecognizer:tapGesture];
        }
        headerView = self.header;
    }
    else if (sectionObj.typeID == 1) {
        EmptyHeaderView *dView = [EmptyHeaderView instanceWithNibName:nil bundle:nil owner:nil];
        if (dView) {
            //            dView
        }
        headerView = dView;
    }
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
   
    static NSString *identifier;

    if (dLTableRow.typeID==CommonActionRow)
    {
        identifier = @"CommonActionCell";
        CommonActionCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 5);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.actionButton.tag=indexPath.section;
        [cell.actionButton addTarget:self action:@selector(saveButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else{
        identifier = @"CommonInputCell";
        CommonInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text= dLTableRow.rowValue;
        
        if (![cell.inputField.allTargets containsObject:self]) {
            [cell.inputField addTarget:self action:@selector(textFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
        }
        
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    
    if (sectionObj.typeID == 0) { // Massage for
        NSMutableDictionary *info = [sectionObj.sectionInfo mutableCopy];
        info[@"title"] = sectionObj.rowsData[indexPath.row];
        sectionObj.sectionInfo = [info copy];
        
        if(indexPath.section==0){
            //[self updateSectionsByMassageForIndex:indexPath.row];
        }
    }
    
    [self tableView:tableView toggleSection:indexPath.section];*/
}

//------------------------------------------------------
#pragma mark - TableView Utils (Private)
//------------------------------------------------------

-(void) tableView:(UITableView *) tableView toggleSection:(NSInteger) section {
    AKDTableSection *sectionObj = self.tableData[section];
    sectionObj.isCollapsed = !sectionObj.isCollapsed;
    
    [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void) tappedOnHeader:(UIGestureRecognizer *) gesture {
    UIView *header = gesture.view;
    if ([header isKindOfClass:[PhotoHeaderView class]]) {
        //[self tableView:self.tableView toggleSection:((PhotoHeaderView *) header).section];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 95;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

//----------------------------------------------------------------
#pragma mark - API -
#pragma mark Password Change
//----------------------------------------------------------------

-(void)requestToChangePassword:(AKDTableSection *)section
{
    
    NSDictionary *param = @{
                            @"UpdatePassword":@"true",
                            @"CurrentPassword" : ((DLTableRow *)section.rowsData[0]).rowValue,
                            @"NewPassword" : ((DLTableRow *)section.rowsData[1]).rowValue
                            };
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsAgentMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Password Successfully updated."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

#pragma mark Info Change

-(void)requestToChangeAccountInfo:(AKDTableSection *)section
{
    NSDictionary *param = @{
                            @"UpdateAccountInfo":@"true",
                            @"FirstName" : ((DLTableRow *)section.rowsData[0]).rowValue,
                            @"LastName" : ((DLTableRow *)section.rowsData[1]).rowValue,
                            @"LicensingNumber" : ((DLTableRow *)section.rowsData[2]).rowValue,
                            @"BrokerNumber" : ((DLTableRow *)section.rowsData[3]).rowValue,
                            @"Address" : ((DLTableRow *)section.rowsData[4]).rowValue,
                            @"OfficePhone" : ((DLTableRow *)section.rowsData[5]).rowValue,
                            @"MobilePhone" : ((DLTableRow *)section.rowsData[6]).rowValue,
                            @"AdditionalRecipient" : ((DLTableRow *)section.rowsData[7]).rowValue
                            };
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsAgentMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Account Information Successfully updated."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void)requestToChangeAccountFooterInfo:(AKDTableSection *)section
{
    NSDictionary *param = @{
                            @"UpdateAccountFooter":@"true",
                            @"Company" : ((DLTableRow *)section.rowsData[0]).rowValue,
                            @"Website" : ((DLTableRow *)section.rowsData[1]).rowValue,
                            @"Facebook" : ((DLTableRow *)section.rowsData[2]).rowValue,
                            @"Twitter" : ((DLTableRow *)section.rowsData[3]).rowValue,
                            @"LinkedIn" : ((DLTableRow *)section.rowsData[4]).rowValue,
                            };
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsAgentMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Account Information Successfully updated."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

#pragma mark - uploadPhoto

-(void)uploadPhoto:(UIImage *)image
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    AFHTTPRequestOperation *op = [[AFILoanRequestManager manager] POST:@"SettingsBrokerMobile" parameters:@{@"UpdatePhoto":@"true"} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData
                                    name:@"photo"
                                fileName:@"photo.jpeg"
                                mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *JSON = (NSDictionary *)responseObject;
        NSLog(@"responseObject = %@",JSON);
        [SVProgressHUD showSuccessWithStatus:@"Photo Successfully uploaded."];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
    
    [op setUploadProgressBlock:^(NSUInteger bytesWritten,
                                 long long totalBytesWritten,
                                 long long totalBytesExpectedToWrite) {
        NSLog(@"Wrote %ld/%ld", (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
    }];
    
    [op start];
}

#pragma mark - UITextField Value change Inspector
-(void) textFieldTextChanged:(UITextField *)sender
{
    CommonInputCell *cell = (CommonInputCell *)[sender firstSuperviewOfClass:[CommonInputCell class]];
    if (cell) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        AKDTableSection *sectionObj = self.tableData[indexPath.section];
        DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
        dLTableRow.rowValue = sender.text;
    }
}

//=================================================================
#pragma mark - ActionSheet Delegates
//=================================================================
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self openWithSourceType:UIImagePickerControllerSourceTypeCamera];
            break;
        case 1:
            [self openWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        case 2:
            NSLog(@"Cancel.");
            break;
            
        default:
            break;
    }
}

-(void)openWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    pickerController.allowsEditing = NO;
    pickerController.sourceType = sourceType;
    pickerController.modalPresentationStyle = UIModalPresentationFullScreen;
    
    if (IS_IOS_8_OR_LATER) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self presentViewController:pickerController animated:YES completion:nil];
        }];
    }
    else
        [self presentViewController:pickerController animated:YES completion:nil];
}

//=================================================================
#pragma mark - ImagePicker Delegates
//=================================================================

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.header.imageView.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self uploadPhoto:chosenImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

@end
