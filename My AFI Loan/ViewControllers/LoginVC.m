//
//  LoginVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "LoginVC.h"
#import "MainVC.h"

@interface LoginVC (){
}

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_userNameTextField setPadding];
    [_passwordTextField setPadding];
    //loan officer
    //_userNameTextField.text=@"Lo";
    //_passwordTextField.text=@"RTP!80";
    
    //borrower
  // _userNameTextField.text=@"DemoBorrowerAFI@gmail.com";
   // _passwordTextField.text=@"1";
    
    //Agent
  // _userNameTextField.text=@"DemoAgentAFI@gmail.com";
    //_passwordTextField.text=@"1";
   
    //Broker
    //_userNameTextField.text=@"acmerealty";
    //_passwordTextField.text=@"abc123!";
   //
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)loginButtonTapAction:(id)sender{
    [self loginUser:_userNameTextField.text password:_passwordTextField.text];
    
   /* UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainVC *mainVC=[storyboard instantiateViewControllerWithIdentifier:@"Home_VC_ID"];
    [self.navigationController setViewControllers:@[mainVC] animated:YES];*/
}
-(void)loginUser:(NSString*)userName password:(NSString*)password
{
    
    [[[AFILoanRequestManager manager] reachabilityManager] isReachable];
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager] GET:@"HomeMobile" parameters:@{@"Login":@"true",@"Username":userName,@"Password":password} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get info : %@", responseObject);
        [SVProgressHUD dismiss];
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                MainVC *mainVC=[storyboard instantiateViewControllerWithIdentifier:@"Home_VC_ID"];
                [[AppUserDefault sharedInstance] setUserDefaultStringValue:[[result objectForKey:@"Results"] valueForKey:@"HeaderAuthentication"] key:sessionKey];
                [[AppUserDefault sharedInstance] setUserDefaultIntegerValue:[[[result objectForKey:@"Results"] valueForKey:@"usertype"] integerValue] key:userTypeKey];
                
                [self.navigationController setViewControllers:@[mainVC] animated:YES];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting signing info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
