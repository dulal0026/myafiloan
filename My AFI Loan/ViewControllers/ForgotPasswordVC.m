//
//  ForgotPasswordVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "AlertManager.h"
#import "ApplicationTextManager.h"

@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_emailTextField setPadding];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)forgotPasswordButtonTapAction:(id)sender{
    NSLog(@"forgotPasswordButtonTapAction");
    if ([self.emailTextField.text isEqualToString:@""]) {
         [AlertManager showAlertSingle:@"" msg:emailEmptyMsg];
    }
    else{
        if ([UtilityManager validateEmail:self.emailTextField.text]) {
            
            if ([AFNetworkReachabilityManager sharedManager].reachable)
                [self requestToResetPassword:self.emailTextField.text];
            else [AlertManager showAlertSingle:@"Connection Error" msg:noInternetAlertMsg];
        }
        else{
            [AlertManager showAlertSingle:@"" msg:emailValidationMsg];
        }
    }
}

//----------------------------------------------------------------
#pragma mark - API -
#pragma mark Password Reset
//----------------------------------------------------------------

-(void)requestToResetPassword:(NSString *)string
{
    NSDictionary *param = @{
                            @"ResetPassword":@"true",
                            @"Email" : string
                            };
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"HomeMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Reset Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Password Successfully Reseted."];
                self.emailTextField.text=@"";
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
