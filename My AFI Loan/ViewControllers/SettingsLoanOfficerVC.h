//
//  SettingsLoanOfficerVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/20/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentDetailsCell.h"
#import "ReinviteCell.h"
#import "UIViewController+MMDrawerController.h"
#import "SVProgressHUD.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"
#import "ApplicationTextManager.h"
#import "AKDTableSection.h"
#import "DLTableRow.h"
#import "UIImageView+WebCache.h"
#import "AlertManager.h"
#import "InputValidator.h"
#import "ViewUtils.h"
#import "Constants.h"

@interface SettingsLoanOfficerVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSMutableArray *tableData;
-(IBAction)showMenu:(id)sender;
-(IBAction)uploadPhotoButtonAction:(id)sender;
@end
