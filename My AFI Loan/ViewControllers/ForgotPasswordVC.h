//
//  ForgotPasswordVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+CustomBorder.h"
#import "UtilityManager.h"
#import "SVProgressHUD.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"
#import "AlertManager.h"
#import "ApplicationTextManager.h"

@interface ForgotPasswordVC : UIViewController
@property(nonatomic,strong)IBOutlet UIButton *forgotPasswordButton;
@property(nonatomic,strong)IBOutlet UITextField *emailTextField;
-(IBAction)forgotPasswordButtonTapAction:(id)sender;
-(IBAction)goBack:(id)sender;
@end
