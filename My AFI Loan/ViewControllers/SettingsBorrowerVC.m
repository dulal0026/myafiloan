//
//  SettingsBorrowerVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/16/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "SettingsBorrowerVC.h"
#import "ApplicationTextManager.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"
#import "SVProgressHUD.h"
#import "AlertManager.h"
#import "InputValidator.h"

@interface SettingsBorrowerVC ()

@end

@implementation SettingsBorrowerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_olddPasswordTextField setPadding];
    [_neewPasswordTextField setPadding];
    [_repeatPasswordTextField setPadding];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}
-(IBAction)saveButtonAction:(id)sender {
  
    if ([InputValidator isPasswordFormValid:_olddPasswordTextField.text
                                    newpass:_neewPasswordTextField.text
                                     rePass:_repeatPasswordTextField.text]) {
        
        [self requestToChangePassword];
    }
}

//----------------------------------------------------------------
#pragma mark - API -
#pragma mark Password Change
//----------------------------------------------------------------

-(void)requestToChangePassword
{
    NSDictionary *param = @{
                            @"UpdatePassword":@"true",
                            @"CurrentPassword" : _olddPasswordTextField.text,
                            @"NewPassword" : _neewPasswordTextField.text
                            };
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"SettingsBorrowerMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update Password : %@", responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *result = responseObject;
            
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
               
                [self emptyTextFields];
                 [SVProgressHUD showSuccessWithStatus:@"Password Successfully updated."];
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

-(void)emptyTextFields
{
    _olddPasswordTextField.text = @"";
    _neewPasswordTextField.text = @"";
    _repeatPasswordTextField.text = @"";
}

@end
