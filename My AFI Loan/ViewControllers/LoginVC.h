//
//  LoginVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+CustomBorder.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"
#import "ApplicationTextManager.h"
#import "SVProgressHUD.h"


@interface LoginVC : UIViewController
@property(nonatomic,strong)IBOutlet UIButton *loginButton;
@property(nonatomic,strong)IBOutlet UIButton *forgotPasswordButton;
@property(nonatomic,strong)IBOutlet UITextField *userNameTextField;
@property(nonatomic,strong)IBOutlet UITextField *passwordTextField;
-(IBAction)loginButtonTapAction:(id)sender;
@end
