//
//  NewLoanVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/30/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "AKDTableSection.h"
#import "EmptyHeaderView.h"
#import "ViewUtils.h"
#import "OptionalFieldHeader.h"
#import "CommonActionCell.h"
#import "CommonInputCell.h"
#import "DLTableRow.h"
#import "CommonInputSwitchCell.h"
#import "Constants.h"
#import "AFILoanRequestManager.h"
#import "SVProgressHUD.h"
#import "AppUserDefault.h"
#import "ApplicationTextManager.h"
#import "AlertManager.h"
#import "InputValidator.h"
#import "IQActionSheetPickerView.h"
#import "DOBInputCell.h"

@interface NewLoanVC : UIViewController<IQActionSheetPickerViewDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;

- (IBAction)showMenu:(id)sender;
-(IBAction)goBack:(id)sender;
@end
