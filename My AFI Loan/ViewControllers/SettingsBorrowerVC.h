//
//  SettingsBorrowerVC.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/16/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+CustomBorder.h"
#import "UIViewController+MMDrawerController.h"

@interface SettingsBorrowerVC : UIViewController

-(IBAction)showMenu:(id)sender;
-(IBAction)saveButtonAction:(id)sender;
@property(nonatomic,strong)IBOutlet UIButton *saveButton;
@property(nonatomic,strong)IBOutlet UITextField *olddPasswordTextField;
@property(nonatomic,strong)IBOutlet UITextField *neewPasswordTextField;
@property(nonatomic,strong)IBOutlet UITextField *repeatPasswordTextField;
@end
