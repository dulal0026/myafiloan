//
//  LoanDetailsVC.m
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/16/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#define SYSTEM_VERSION                              ([[UIDevice currentDevice] systemVersion])
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IS_IOS8_OR_ABOVE                            (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))

#import "LoanDetailsVC.h"
#import "AccountHeader.h"
#import "PrequalTriOptionCell.h"
#import "AFNetworkReachabilityManager.h"
#import "AlertManager.h"
#import "SVProgressHUD.h"
#import "AFILoanRequestManager.h"
#import "AppUserDefault.h"

typedef NS_ENUM(NSInteger, HeaderType) {
    loanHeader,
    agentHeader,
    monthlyExpensesHeader,
    accountHeader
};
typedef NS_ENUM(NSInteger, SectionRowType) {
    loanRow,
    agentRow,
    switchRow,
    commonInputRow,
    monthlyFeesRow,
    disabledInputRow,
    commonActionRow,
    prequalSwitchRow,
    prequalTriOptionRow,
    lsuSwitchRow
};

@interface LoanDetailsVC ()
{
    AgentModificationStatus *agentModificationStatus;
}
@property (nonatomic, strong) PrequalTriOptionCell *prequalTriOptionCell;
@property (nonatomic, strong) CommonInputSwitchCell *commonInputSwitchCell;
@property (nonatomic, strong) LoanContactCell *loanContactCell;
@property (nonatomic, strong) AgentCell *agentCell;
@property (nonatomic, strong) CommonInputCell *commonInputCell;
@property (nonatomic, strong) MonthlyFeesInputCell *monthlyFeesInputCell;
@property (nonatomic, strong) DisabledInputCell *disabledInputCell;
@property (nonatomic, strong) CommonActionCell *commonActionCell;
@property (nonatomic, strong) GraySwitchCell *graySwitchCell;

@end

@implementation LoanDetailsVC

//------------------------------------------------------
#pragma mark - ViewController LifeCycle
//------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"LoanContactCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LoanContactCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AgentCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AgentCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CommonInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonInputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"MonthlyFeesInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MonthlyFeesInputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"PrequalTriOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PrequalTriOptionCell"];
     [self.tableView registerNib:[UINib nibWithNibName:@"CommonActionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonActionCell"];
    
     [self.tableView registerNib:[UINib nibWithNibName:@"CommonInputSwitchCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommonInputSwitchCell"];
     [self.tableView registerNib:[UINib nibWithNibName:@"DisabledInputCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DisabledInputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"GraySwitchCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GraySwitchCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AccountHeader class]) bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:NSStringFromClass([AccountHeader class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([OptionalFieldHeader class]) bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:NSStringFromClass([OptionalFieldHeader class])];
    
    agentModificationStatus = AgentModificationStatusNone;
    [self loadTableViewData:-1];
 
    self.loanNoLabel.text=[NSString stringWithFormat:@"LOAN %@",self.loanNo];
}

-(PrequalTriOptionCell *)prequalTriOptionCell {
    if (!_prequalTriOptionCell) {
        _prequalTriOptionCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PrequalTriOptionCell class])];
    }
    return _prequalTriOptionCell;
}

-(CommonInputCell *)commonInputCell {
    if (!_commonInputCell) {
        _commonInputCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommonInputCell class])];
    }
    return _commonInputCell;
}
-(MonthlyFeesInputCell *)monthlyFeesInputCell {
    if (!_monthlyFeesInputCell) {
        _monthlyFeesInputCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MonthlyFeesInputCell class])];
    }
    return _monthlyFeesInputCell;
}
-(GraySwitchCell *)graySwitchCell {
    if (!_graySwitchCell) {
        _graySwitchCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([GraySwitchCell class])];
    }
    return _graySwitchCell;
}

-(CommonInputSwitchCell *)commonInputSwitchCell {
    if (!_commonInputSwitchCell) {
        _commonInputSwitchCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommonInputSwitchCell class])];
    }
    return _commonInputSwitchCell;
}
-(CommonActionCell *)commonActionCell {
    if (!_commonActionCell) {
        _commonActionCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommonActionCell class])];
    }
    return _commonActionCell;
}

-(AgentCell *)agentCell {
    if (!_agentCell) {
        _agentCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AgentCell class])];
    }
    return _agentCell;
}

-(LoanContactCell *)loanContactCell {
    if (!_loanContactCell) {
        _loanContactCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LoanContactCell class])];
    }
    return _loanContactCell;
}

-(void)loadTableViewData:(int)index{
    [self.tableData removeAllObjects];
   
    switch (index) {
        case -1:{
            [self requestForLoanData];
            break;
        }
        case 0:{
            [self requestForPreQualData];
            break;
        }
        case 1:{
            [self requestForLSUData];
            break;
        }
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
//------------------------------------------------------
#pragma mark - API -
#pragma mark - Api methods
//------------------------------------------------------
-(void) requestForLoanData{
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"LoanMobile" parameters:@{@"Loan":@"true",@"LoanNumber":self.loanNo} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get LoanMobile : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                NSMutableDictionary *infoDictionary=[responseObject objectForKey:@"Results"];
                
                // Agent  Section ------------------------------------------------------
                AKDTableSection *agentSection = [AKDTableSection new];
                agentSection.typeID = agentHeader;
                agentSection.sectionInfo = @"Agents";
                agentSection.sectionHeaderHeight = 44;
                agentSection.isCollapsed=NO;
                NSMutableArray *agentRowArray=[NSMutableArray new];
            
                // Loan Contact Section ------------------------------------------------------
                AKDTableSection *loanContactSection = [AKDTableSection new];
                loanContactSection.typeID = loanHeader;
                loanContactSection.sectionInfo = @"Loan Contact";
                loanContactSection.sectionHeaderHeight = 48;
                loanContactSection.isCollapsed=NO;
                NSMutableArray *rowArray=[NSMutableArray new];
                
                NSMutableArray *contactArray=[LoanContactInfoBBA arrayOfModelsFromDictionaries:infoDictionary[@"Contacts"]];
                for (LoanContactInfoBBA *loanContact in contactArray) {
                    //1=loan officer 2= Agent 3=borrower 5=broker
                   
                   // if (loanContact.usertype!=1) {
                    
                    NSInteger userType=[[[AppUserDefault sharedInstance] getStringValueforKey:userTypeKey] integerValue];
               
                    if (userType==1) {
                        if (loanContact.usertype==2) {
                            DLTableRow *dlTableRow40=[DLTableRow new];
                            dlTableRow40.typeID=agentRow;
                            dlTableRow40.rowTitle=loanContact.Name;
                            dlTableRow40.rowValue=loanContact.PhoneNumber;
                            dlTableRow40.email=loanContact.Email;
                            [agentRowArray addObject:dlTableRow40];
                        }
                    }
                    
                       /* else{
                            DLTableRow *dlTableRow40=[DLTableRow new];
                            dlTableRow40.typeID=loanRow;
                            dlTableRow40.rowTitle=loanContact.Name;
                            dlTableRow40.rowValue=loanContact.PhoneNumber;
                            dlTableRow40.email=loanContact.Email;
                            [rowArray addObject:dlTableRow40];
                        }*/
                    //}
                }
                
                loanContactSection.rowsData = rowArray;
                
                agentSection.rowsData = agentRowArray;
                
                self.tableData = [@[
                                    loanContactSection,
                                    agentSection
                                    ] mutableCopy];
                
                self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                [self.tableView reloadData];
                
            }
            else{
                [AlertManager showAlertSingle:@"Error" msg:[[responseObject objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void) requestForPreQualData{
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"PrequalMobile" parameters:@{@"Get":@"true",@"Loan_Number":self.loanNo} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get PrequalMobile : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                 NSDictionary *infoDictionary=[responseObject objectForKey:@"Results"];
                
                // Loan Contact Section ------------------------------------------------------
                AKDTableSection *loanPreQualTopSection = [AKDTableSection new];
                loanPreQualTopSection.typeID = loanHeader;
                loanPreQualTopSection.sectionHeaderHeight = 0;
                loanPreQualTopSection.isCollapsed=NO;
                NSMutableArray *rowArray=[NSMutableArray new];
                
                DLTableRow *dlTableRow=[DLTableRow new];
                dlTableRow.typeID=commonInputRow;
                dlTableRow.rowTitle=@"Max Purchase Price";
                
                double maxPurchasePrice=[[infoDictionary valueForKey:@"Max_Purchase_Price"] doubleValue];
                dlTableRow.rowValue=[NSString stringWithFormat:@"$%.02f",maxPurchasePrice];
                [rowArray addObject:dlTableRow];
                
                DLTableRow *dlTableRow1=[DLTableRow new];
                dlTableRow1.typeID=commonInputRow;
                dlTableRow1.rowTitle=@"Down Payment";
                
                double downPayment=[[infoDictionary valueForKey:@"Down_Payment"] doubleValue];
                dlTableRow1.rowValue=[NSString stringWithFormat:@"$%.02f",downPayment];
                
                [rowArray addObject:dlTableRow1];
                
                DLTableRow *dlTableRow2=[DLTableRow new];
                dlTableRow2.typeID=switchRow;
                dlTableRow2.rowTitle=@"Market Rate";
                dlTableRow2.boolValue=[infoDictionary valueForKey:@"Market_Rate"]==0?YES:NO;
                
                [rowArray addObject:dlTableRow2];
                
                DLTableRow *dlTableRow3=[DLTableRow new];
                dlTableRow3.typeID=commonInputRow;
                dlTableRow3.rowTitle=@"Interest Rate";
                dlTableRow3.rowValue=[infoDictionary valueForKey:@"Interest_Rate"];
                
                double interestRate=[[infoDictionary valueForKey:@"Interest_Rate"] doubleValue];
                dlTableRow3.rowValue=[NSString stringWithFormat:@"$%.04f",interestRate];
                [rowArray addObject:dlTableRow3];
                
                DLTableRow *dlTableRow4=[DLTableRow new];
                dlTableRow4.typeID=commonInputRow;
                dlTableRow4.rowTitle=@"Loan Term";
                dlTableRow4.rowValue=[infoDictionary valueForKey:@"Loan_Term"];
                [rowArray addObject:dlTableRow4];
                
                DLTableRow *dlTableRow5=[DLTableRow new];
                dlTableRow5.typeID=switchRow;
                dlTableRow5.rowTitle=@"UFMIP Financed";
                dlTableRow5.boolValue=[infoDictionary valueForKey:@"UFMIP_Financed"]==0?YES:NO;
                [rowArray addObject:dlTableRow5];
                
                DLTableRow *dlTableRow6=[DLTableRow new];
                dlTableRow6.typeID=commonInputRow;
                dlTableRow6.rowTitle=@"UFMIP Funding Fee";
                
                double UFMIP_Funding_Fee=[[infoDictionary valueForKey:@"UFMIP_Funding_Fee"] doubleValue];
                dlTableRow6.rowValue=[NSString stringWithFormat:@"$%.02f",UFMIP_Funding_Fee];
                
                [rowArray addObject:dlTableRow6];
                
                DLTableRow *dlTableRow7=[DLTableRow new];
                dlTableRow7.typeID=disabledInputRow;
                dlTableRow7.rowTitle=@"Max Loan Amount";
                
                double Max_Loan_Amount=[[infoDictionary valueForKey:@"Max_Loan_Amount"] doubleValue];
                dlTableRow7.rowValue=[NSString stringWithFormat:@"$%.02f",Max_Loan_Amount];
                [rowArray addObject:dlTableRow7];
                
                DLTableRow *dlTableRow8=[DLTableRow new];
                dlTableRow8.typeID=commonInputRow;
                dlTableRow8.rowTitle=@"Actual Purchase Price";
          
                double Actual_Purchase_Price=[[infoDictionary valueForKey:@"Actual_Purchase_Price"] doubleValue];
                dlTableRow8.rowValue=[NSString stringWithFormat:@"$%.02f",Actual_Purchase_Price];
                [rowArray addObject:dlTableRow8];
                
                DLTableRow *dlTableRow9=[DLTableRow new];
                dlTableRow9.typeID=disabledInputRow;
                dlTableRow9.rowTitle=@"Base Loan Amount";
              
                double Base_Loan_Amount=[[infoDictionary valueForKey:@"Base_Loan_Amount"] doubleValue];
                dlTableRow9.rowValue=[NSString stringWithFormat:@"$%.02f",Base_Loan_Amount];
                [rowArray addObject:dlTableRow9];
                
                loanPreQualTopSection.rowsData = rowArray;
                
                // Max Monthly Expenses  Section ------------------------------------------------------
                AKDTableSection *monthlyExpensesSection = [AKDTableSection new];
                monthlyExpensesSection.typeID = monthlyExpensesHeader;
                monthlyExpensesSection.sectionInfo = @"Max Monthly Expenses";
                monthlyExpensesSection.sectionHeaderHeight = 44;
                monthlyExpensesSection.isCollapsed=YES;
                monthlyExpensesSection.inputValue=[infoDictionary objectForKey:@"Max_Monthly_Expenses"];
                NSMutableArray *rowArray2=[NSMutableArray new];
                
                DLTableRow *dlTableRow20=[DLTableRow new];
                dlTableRow20.typeID=monthlyFeesRow;
                dlTableRow20.rowTitle=@"Other Financing P&I";
            
                double Other_Financing_PI=[[infoDictionary valueForKey:@"Other_Financing_PI"] doubleValue];
                dlTableRow20.rowValue=[NSString stringWithFormat:@"$%.02f",Other_Financing_PI];
                [rowArray2 addObject:dlTableRow20];
                
                DLTableRow *dlTableRow21=[DLTableRow new];
                dlTableRow21.typeID=monthlyFeesRow;
                dlTableRow21.rowTitle=@"Hazard Insurance";
             
                double Hazard_Insurance=[[infoDictionary valueForKey:@"Hazard_Insurance"] doubleValue];
                dlTableRow21.rowValue=[NSString stringWithFormat:@"$%.02f",Hazard_Insurance];
                [rowArray2 addObject:dlTableRow21];
                
                DLTableRow *dlTableRow22=[DLTableRow new];
                dlTableRow22.typeID=monthlyFeesRow;
                dlTableRow22.rowTitle=@"Real Estate Taxes";
             
                double Real_Estate_Taxes=[[infoDictionary valueForKey:@"Real_Estate_Taxes"] doubleValue];
                dlTableRow22.rowValue=[NSString stringWithFormat:@"$%.02f",Real_Estate_Taxes];
                [rowArray2 addObject:dlTableRow22];
                
                DLTableRow *dlTableRow23=[DLTableRow new];
                dlTableRow23.typeID=monthlyFeesRow;
                dlTableRow23.rowTitle=@"Mortgage Insurance";
           
                double Mortgage_Insurance_Percent=[[infoDictionary valueForKey:@"Mortgage_Insurance_Percent"] doubleValue];
                dlTableRow23.rowValue=[NSString stringWithFormat:@"$%.02f",Mortgage_Insurance_Percent];
                [rowArray2 addObject:dlTableRow23];
                
                DLTableRow *dlTableRow24=[DLTableRow new];
                dlTableRow24.typeID=monthlyFeesRow;
                dlTableRow24.rowTitle=@"HOA Dues";
                double HOA_Dues=[[infoDictionary valueForKey:@"HOA_Dues"] doubleValue];
                dlTableRow24.rowValue=[NSString stringWithFormat:@"$%.02f",HOA_Dues];
                [rowArray2 addObject:dlTableRow24];
                
                DLTableRow *dlTableRow25=[DLTableRow new];
                dlTableRow25.typeID=monthlyFeesRow;
                dlTableRow25.rowTitle=@"Other Expenses";
                double Other_Expenses=[[infoDictionary valueForKey:@"Other_Expenses"] doubleValue];
                dlTableRow25.rowValue=[NSString stringWithFormat:@"$%.02f",Other_Expenses];
                [rowArray2 addObject:dlTableRow25];
                
                monthlyExpensesSection.rowsData = rowArray2;
                
                // Max Monthly Payment Section ------------------------------------------------------
                AKDTableSection *monthlyPaymentSection = [AKDTableSection new];
                monthlyPaymentSection.typeID = loanHeader;
                monthlyPaymentSection.sectionInfo = @"Agents";
                monthlyPaymentSection.sectionHeaderHeight = 0;
                monthlyPaymentSection.isCollapsed=NO;
                NSMutableArray *rowArray3=[NSMutableArray new];
                
                DLTableRow *dlTableRow30=[DLTableRow new];
                dlTableRow30.typeID=commonInputRow;
                dlTableRow30.rowTitle=@"Monthly Payment";
                double Monthly_Payment=[[infoDictionary valueForKey:@"Monthly_Payment"] doubleValue];
                dlTableRow30.rowValue=[NSString stringWithFormat:@"$%.02f",Monthly_Payment];
                [rowArray3 addObject:dlTableRow30];
                
                monthlyPaymentSection.rowsData = rowArray3;
                
                // Lender Actions Section ------------------------------------------------------
                AKDTableSection *lenderActionsSection = [AKDTableSection new];
                lenderActionsSection.typeID = accountHeader;
                lenderActionsSection.sectionInfo = @"Lender Actions";
                lenderActionsSection.sectionHeaderHeight = 48;
                lenderActionsSection.isCollapsed=NO;
                NSMutableArray *lenderActions=[NSMutableArray new];
                
                DLTableRow *dlTableRow40=[DLTableRow new];
                dlTableRow40.typeID=prequalSwitchRow;
                dlTableRow40.rowTitle=@"Buyer is relying on the sale or lease of a property to qualify for this loan.";
                dlTableRow40.boolValue=[[infoDictionary objectForKey:@"Relying_On_Sale"] intValue]==1?YES:NO;
                [lenderActions addObject:dlTableRow40];
                
                DLTableRow *dlTableRow41=[DLTableRow new];
                dlTableRow41.typeID=prequalSwitchRow;
                dlTableRow41.rowTitle=@"Buyer is relying on Seller Concessions for Buyer's loan costs.";
                dlTableRow41.boolValue=[[infoDictionary objectForKey:@"Relying_On_Seller_Concessions"] intValue]==1?YES:NO;
                [lenderActions addObject:dlTableRow41];
                
                DLTableRow *dlTableRow42=[DLTableRow new];
                dlTableRow42.typeID=prequalTriOptionRow;
                dlTableRow42.rowTitle=@"Lender has provided Buyer with the HUD form.";
                dlTableRow42.rowValue=[infoDictionary objectForKey:@"Hud"];
                [lenderActions addObject:dlTableRow42];
                
                DLTableRow *dlTableRow43=[DLTableRow new];
                dlTableRow43.typeID=prequalTriOptionRow;
                dlTableRow43.rowTitle=@"Lender has completed a verbal discussion with Buyer including income, assets and debts.";
                dlTableRow43.rowValue=[infoDictionary objectForKey:@"Verbal"];
                [lenderActions addObject:dlTableRow43];
                
                DLTableRow *dlTableRow44=[DLTableRow new];
                dlTableRow44.typeID=prequalTriOptionRow;
                dlTableRow44.rowTitle=@"Lender has obtained a Tri-Merged Residential Credit Report";
                dlTableRow44.rowValue=[infoDictionary objectForKey:@"Tri"];
                [lenderActions addObject:dlTableRow44];
                
                lenderActionsSection.rowsData = lenderActions;
         
                // Requested Document  Section ------------------------------------------------------
                AKDTableSection *requestedDocumentSection = [AKDTableSection new];
                requestedDocumentSection.typeID = accountHeader;
                requestedDocumentSection.sectionInfo = @"Requested Documents";
                requestedDocumentSection.sectionHeaderHeight = 48;
                requestedDocumentSection.isCollapsed=NO;
                
                NSMutableArray *requestedDocuments=[NSMutableArray new];
                
                DLTableRow *dlTableRow50=[DLTableRow new];
                dlTableRow50.typeID=prequalTriOptionRow;
                dlTableRow50.rowTitle=@"Paystubs";
                dlTableRow50.rowValue=[infoDictionary objectForKey:@"PayStub"];
                [requestedDocuments addObject:dlTableRow50];
                
                DLTableRow *dlTableRow51=[DLTableRow new];
                dlTableRow51.typeID=prequalTriOptionRow;
                dlTableRow51.rowTitle=@"Down Payment/Reserves Documentation";
                dlTableRow51.rowValue=[infoDictionary objectForKey:@"Down_Payment_Doc"];
                [requestedDocuments addObject:dlTableRow51];
                
                DLTableRow *dlTableRow52=[DLTableRow new];
                dlTableRow52.typeID=prequalTriOptionRow;
                dlTableRow52.rowTitle=@"W-2s";
                dlTableRow52.rowValue=[infoDictionary objectForKey:@"W2"];
                [requestedDocuments addObject:dlTableRow52];
                
                DLTableRow *dlTableRow53=[DLTableRow new];
                dlTableRow53.typeID=prequalTriOptionRow;
                dlTableRow53.rowTitle=@"Gift Documentation";
                dlTableRow53.rowValue=[infoDictionary objectForKey:@"Gift_Documentation"];
                [requestedDocuments addObject:dlTableRow53];
                
                DLTableRow *dlTableRow54=[DLTableRow new];
                dlTableRow54.typeID=prequalTriOptionRow;
                dlTableRow54.rowTitle=@"Personal Tax Returns";
                dlTableRow54.rowValue=[infoDictionary objectForKey:@"Personal_Tax_Return"];
                [requestedDocuments addObject:dlTableRow54];
                
                DLTableRow *dlTableRow55=[DLTableRow new];
                dlTableRow55.typeID=prequalTriOptionRow;
                dlTableRow55.rowTitle=@"Corporate Tax Returns";
                dlTableRow55.rowValue=[infoDictionary objectForKey:@"Corporate_Tax_Return"];
                [requestedDocuments addObject:dlTableRow55];
                
                DLTableRow *dlTableRow56=[DLTableRow new];
                dlTableRow56.typeID=prequalTriOptionRow;
                dlTableRow56.rowTitle=@"Other";
                dlTableRow56.rowValue=[infoDictionary objectForKey:@"Other"];
                [requestedDocuments addObject:dlTableRow56];
                
                requestedDocumentSection.rowsData = requestedDocuments;
             
                AKDTableSection *saveSection = [AKDTableSection new];
                monthlyPaymentSection.typeID = loanHeader;
                monthlyPaymentSection.sectionInfo = @"";
                monthlyPaymentSection.sectionHeaderHeight = 0;
                monthlyPaymentSection.isCollapsed=NO;
                NSMutableArray *saveRows=[NSMutableArray new];
                
                DLTableRow *dlTableRow60=[DLTableRow new];
                dlTableRow60.typeID=commonActionRow;
                [saveRows addObject:dlTableRow60];
                
                saveSection.rowsData=saveRows;
                
                self.tableData = [@[
                                    loanPreQualTopSection,
                                    monthlyExpensesSection,
                                    monthlyPaymentSection,
                                    lenderActionsSection,
                                    requestedDocumentSection,
                                    saveSection
                                    ] mutableCopy];
                
                self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                [self.tableView reloadData];
         
            }
            else{
                [AlertManager showAlertSingle:@"Error" msg:[[responseObject objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void) requestForLSUData{
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"LSUMobile" parameters:@{@"Get":@"true",@"Loan_Number":self.loanNo} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get LSU Data : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                NSDictionary *infoDictionary=[responseObject objectForKey:@"Results"];
                // Lender Actions ------------------------------------------------------
                AKDTableSection *documentationSection = [AKDTableSection new];
                documentationSection.typeID = accountHeader;
                documentationSection.sectionInfo = @"Documentation";
                documentationSection.sectionHeaderHeight = 48;
                documentationSection.isCollapsed=NO;
                NSMutableArray *rowArray=[NSMutableArray new];
                
                DLTableRow *dlTableRow1=[DLTableRow new];
                dlTableRow1.typeID=lsuSwitchRow;
                dlTableRow1.rowTitle=@"Lender has received the Contract and all Addenda.";
                NSLog(@"Documentation_1:%@",[infoDictionary valueForKey:@"Documentation_1"]);
                dlTableRow1.boolValue=[[infoDictionary valueForKey:@"Documentation_1"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow1];
                
                DLTableRow *dlTableRow2=[DLTableRow new];
                dlTableRow2.typeID=lsuSwitchRow;
                dlTableRow2.rowTitle=@"Lender has sent initial Good Faith Estimate and Truth in Lending Disclosures.";
                dlTableRow2.boolValue=[[infoDictionary valueForKey:@"Documentation_2"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow2];
                
                DLTableRow *dlTableRow3=[DLTableRow new];
                dlTableRow3.typeID=lsuSwitchRow;
                dlTableRow3.rowTitle=@"Lender has received a signed Application/1003 and disclosures.";
                dlTableRow3.boolValue=[[infoDictionary valueForKey:@"Documentation_3"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow3];
                
                DLTableRow *dlTableRow4=[DLTableRow new];
                dlTableRow4.typeID=lsuSwitchRow;
                dlTableRow4.rowTitle=@"Lender has identified down payment source.";
                dlTableRow4.boolValue=[[infoDictionary valueForKey:@"Documentation_4"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow4];
                
                DLTableRow *dlTableRow5=[DLTableRow new];
                dlTableRow5.typeID=lsuSwitchRow;
                dlTableRow5.rowTitle=@"Lender has received and reviewed the Title Commitment.";
                dlTableRow5.boolValue=[[infoDictionary valueForKey:@"Documentation_5"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow5];
                
                DLTableRow *dlTableRow6=[DLTableRow new];
                dlTableRow6.typeID=lsuSwitchRow;
                dlTableRow6.rowTitle=@"Payment for the appraisal has been received.";
                dlTableRow6.boolValue=[[infoDictionary valueForKey:@"Documentation_6"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow6];
                
                DLTableRow *dlTableRow7=[DLTableRow new];
                dlTableRow7.typeID=lsuSwitchRow;
                dlTableRow7.rowTitle=@"Lender has ordered the appraisal.";
                dlTableRow7.boolValue=[[infoDictionary valueForKey:@"Documentation_7"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow7];
                
                DLTableRow *dlTableRow8=[DLTableRow new];
                dlTableRow8.typeID=lsuSwitchRow;
                dlTableRow8.rowTitle=@"Buyer has locked the interest rate and points with Lender.";
                dlTableRow8.boolValue=[[infoDictionary valueForKey:@"Documentation_8"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow8];
                
                DLTableRow *dlTableRow9=[DLTableRow new];
                dlTableRow9.typeID=monthlyFeesRow;
                dlTableRow9.rowTitle=@"Lock Expiration Date.";
                dlTableRow9.rowValue=[infoDictionary valueForKey:@"Documentation_9_Lock_Expiration_Date"];
                dlTableRow9.boolValue=NO;
                [rowArray addObject:dlTableRow9];
                
                DLTableRow *dlTableRow10=[DLTableRow new];
                dlTableRow10.typeID=lsuSwitchRow;
                dlTableRow10.rowTitle=@"Lender has received the Initial Requested Documentation.";
                dlTableRow10.boolValue=[[infoDictionary valueForKey:@"Documentation_10"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow10];
                
                DLTableRow *dlTableRow11=[DLTableRow new];
                dlTableRow11.typeID=lsuSwitchRow;
                dlTableRow11.rowTitle=@"Appraisal received and the Premises/Property appraised for at least the purchase price.";
                dlTableRow11.boolValue=[[infoDictionary valueForKey:@"Documentation_11"] intValue]==1?YES:NO;
                [rowArray addObject:dlTableRow11];
                
                documentationSection.rowsData = rowArray;
                
                // Requested Documents ------------------------------------------------------
                AKDTableSection *underwritingSection = [AKDTableSection new];
                underwritingSection.typeID = accountHeader;
                underwritingSection.sectionInfo = @"Underwriting";
                underwritingSection.sectionHeaderHeight = 44;
                underwritingSection.isCollapsed=NO;
                NSMutableArray *rowArray2=[NSMutableArray new];
                
                DLTableRow *dlTableRow21=[DLTableRow new];
                dlTableRow21.typeID=lsuSwitchRow;
                dlTableRow21.rowTitle=@"Paystubs";
                dlTableRow21.boolValue=[[infoDictionary valueForKey:@"Underwriting_1"] intValue]==1?YES:NO;
                [rowArray2 addObject:dlTableRow21];
                
                DLTableRow *dlTableRow22=[DLTableRow new];
                dlTableRow22.typeID=lsuSwitchRow;
                dlTableRow22.rowTitle=@"Down Payment/Reserves Documentation";
                dlTableRow22.boolValue=[[infoDictionary valueForKey:@"Underwriting_2"] intValue]==1?YES:NO;
                [rowArray2 addObject:dlTableRow22];
                
                DLTableRow *dlTableRow23=[DLTableRow new];
                dlTableRow23.typeID=lsuSwitchRow;
                dlTableRow23.rowTitle=@"Paystubs";
                dlTableRow23.boolValue=[[infoDictionary valueForKey:@"Underwriting_3"] intValue]==1?YES:NO;
                [rowArray2 addObject:dlTableRow23];
                
                DLTableRow *dlTableRow24=[DLTableRow new];
                dlTableRow24.typeID=lsuSwitchRow;
                dlTableRow24.rowTitle=@"Down Payment/Reserves Documentation";
                dlTableRow24.boolValue=[[infoDictionary valueForKey:@"Underwriting_4"] intValue]==1?YES:NO;
                [rowArray2 addObject:dlTableRow24];
                
                underwritingSection.rowsData = rowArray2;
                
                
                // Closing Section ------------------------------------------------------
                AKDTableSection *closingSection = [AKDTableSection new];
                closingSection.typeID = accountHeader;
                closingSection.sectionInfo = @"Closing";
                closingSection.sectionHeaderHeight = 44;
                closingSection.isCollapsed=NO;
                NSMutableArray *rowArray3=[NSMutableArray new];
                
                DLTableRow *dlTableRow31=[DLTableRow new];
                dlTableRow31.typeID=lsuSwitchRow;
                dlTableRow31.rowTitle=@"Lender has ordered the Closing Loan Documents and Instructions.";
                dlTableRow31.boolValue=[[infoDictionary valueForKey:@"Closing_1"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow31];
                
                DLTableRow *dlTableRow32=[DLTableRow new];
                dlTableRow32.typeID=lsuSwitchRow;
                dlTableRow32.rowTitle=@"Lender has sent the DOCs to the Escrow Company.";
                 dlTableRow32.boolValue=[[infoDictionary valueForKey:@"Closing_2"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow32];
                
                DLTableRow *dlTableRow33=[DLTableRow new];
                dlTableRow33.typeID=lsuSwitchRow;
                dlTableRow33.rowTitle=@"Lender has received the pre-audit from Escrow Company.";
                 dlTableRow33.boolValue=[[infoDictionary valueForKey:@"Closing_3"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow33];
                
                DLTableRow *dlTableRow34=[DLTableRow new];
                dlTableRow34.typeID=lsuSwitchRow;
                dlTableRow34.rowTitle=@"Lender has approved the pre-audit from Escrow Company.";
                 dlTableRow34.boolValue=[[infoDictionary valueForKey:@"Closing_4"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow34];
                
                DLTableRow *dlTableRow35=[DLTableRow new];
                dlTableRow35.typeID=lsuSwitchRow;
                dlTableRow35.rowTitle=@"Lender has received signed DOCs from all parties.";
                 dlTableRow35.boolValue=[[infoDictionary valueForKey:@"Closing_5"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow35];
                
                DLTableRow *dlTableRow36=[DLTableRow new];
                dlTableRow36.typeID=lsuSwitchRow;
                dlTableRow36.rowTitle=@"All lender Quality Control Reviews have been completed.";
                 dlTableRow36.boolValue=[[infoDictionary valueForKey:@"Closing_6"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow36];
                
                DLTableRow *dlTableRow37=[DLTableRow new];
                dlTableRow37.typeID=lsuSwitchRow;
                dlTableRow37.rowTitle=@"All PFT Conditions have been met and buyer has obtained loan approval without conditions.";
                 dlTableRow37.boolValue=[[infoDictionary valueForKey:@"Closing_7"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow37];
                
                DLTableRow *dlTableRow38=[DLTableRow new];
                dlTableRow38.typeID=lsuSwitchRow;
                dlTableRow38.rowTitle=@"Funds have been ordered.";
                 dlTableRow38.boolValue=[[infoDictionary valueForKey:@"Closing_8"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow38];
                
                DLTableRow *dlTableRow39=[DLTableRow new];
                dlTableRow39.typeID=lsuSwitchRow;
                dlTableRow39.rowTitle=@"All funds have been received by Escrow Company.";
                 dlTableRow39.boolValue=[[infoDictionary valueForKey:@"Closing_9"] intValue]==1?YES:NO;
                [rowArray3 addObject:dlTableRow39];
              
                closingSection.rowsData = rowArray3;
                
                AKDTableSection *lsuSaveSection = [AKDTableSection new];
                lsuSaveSection.typeID = accountHeader;
                lsuSaveSection.sectionInfo = @"";
                lsuSaveSection.sectionHeaderHeight = 0;
                lsuSaveSection.isCollapsed=NO;
                
                NSMutableArray *rowArray4=[NSMutableArray new];
                
                DLTableRow *dlTableRow40=[DLTableRow new];
                dlTableRow40.typeID=commonActionRow;
                dlTableRow40.rowTitle=@"";
                dlTableRow40.rowValue=@"";
                
                [rowArray4 addObject:dlTableRow40];
                
                lsuSaveSection.rowsData = rowArray4;
                
                self.tableData = [@[
                                    documentationSection,
                                    underwritingSection,
                                    closingSection,
                                    lsuSaveSection
                                    ] mutableCopy];
                
                self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                [self.tableView reloadData];
            }
            else [AlertManager showAlertSingle:@"Error" msg:[[responseObject objectForKey:@"General"] valueForKey:@"message"]];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void)requestToSavePrequal:(NSString*)NotifyBorrower{
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    AKDTableSection *loanPreQualTopSectionObj = self.tableData[0];
    AKDTableSection *monthlyExpensesSectionObj = self.tableData[1];
    //AKDTableSection *monthlyPaymentSectionObj = self.tableData[2];
    AKDTableSection *lenderActionSectionObj = self.tableData[3];
    AKDTableSection *requestedDocumentSectionObj = self.tableData[4];
    
    
    NSDictionary *param = @{
                            @"Save":@"true",
                            @"Loan_Number" :self.loanNo,
                            @"Max_Purchase_Price" : ((DLTableRow *)loanPreQualTopSectionObj.rowsData[0]).rowValue,
                            @"Down_Payment" : ((DLTableRow *)loanPreQualTopSectionObj.rowsData[1]).rowValue,
                            @"Market_Rate" :((DLTableRow *)loanPreQualTopSectionObj.rowsData[2]).boolValue?@"true":@"false",
                            @"Interest_Rate" : ((DLTableRow *)loanPreQualTopSectionObj.rowsData[3]).rowValue,
                            @"Loan_Term" :((DLTableRow *)loanPreQualTopSectionObj.rowsData[4]).rowValue,
                            @"UFMIP_Financed" : ((DLTableRow *)loanPreQualTopSectionObj.rowsData[5]).boolValue?@"true":@"false",
                            @"UFMIP_Funding_Fee" : ((DLTableRow *)loanPreQualTopSectionObj.rowsData[6]).rowValue,
                            @"Actual_Purchase_Price" : ((DLTableRow *)loanPreQualTopSectionObj.rowsData[8]).rowValue,
                            
                            @"Other_Financing_PI" : ((DLTableRow *)monthlyExpensesSectionObj.rowsData[0]).rowValue,
                            @"Hazard_Insurance" :((DLTableRow *)monthlyExpensesSectionObj.rowsData[1]).rowValue,
                            @"Real_Estate_Taxes" : ((DLTableRow *)monthlyExpensesSectionObj.rowsData[2]).rowValue,
                            @"Mortgage_Insurance_Percent" : ((DLTableRow *)monthlyExpensesSectionObj.rowsData[3]).rowValue,
                            @"HOA_Dues" : ((DLTableRow *)monthlyExpensesSectionObj.rowsData[4]).rowValue,
                            @"Other_Expenses" : ((DLTableRow *)monthlyExpensesSectionObj.rowsData[5]).rowValue,
                        
                            @"Relying_On_Sale" : ((DLTableRow *)lenderActionSectionObj.rowsData[0]).boolValue?@"true":@"false",
                            @"Relying_On_Seller_Concessions" : ((DLTableRow *)lenderActionSectionObj.rowsData[1]).boolValue?@"true":@"false",
                            @"Hud" : ((DLTableRow *)lenderActionSectionObj.rowsData[2]).rowValue,
                            @"Verbal" : ((DLTableRow *)lenderActionSectionObj.rowsData[3]).rowValue ,
                            @"Tri" : ((DLTableRow *)lenderActionSectionObj.rowsData[4]).rowValue,
                        
                            @"PayStub" : ((DLTableRow *)requestedDocumentSectionObj.rowsData[0]).rowValue,
                            @"Down_Payment_Doc" : ((DLTableRow *)requestedDocumentSectionObj.rowsData[1]).rowValue,
                            @"W2" : ((DLTableRow *)requestedDocumentSectionObj.rowsData[2]).rowValue,
                            @"Gift_Documentation" : ((DLTableRow *)requestedDocumentSectionObj.rowsData[3]).rowValue,
                            @"Personal_Tax_Return" : ((DLTableRow *)requestedDocumentSectionObj.rowsData[4]).rowValue,
                            @"Corporate_Tax_Return" : ((DLTableRow *)requestedDocumentSectionObj.rowsData[5]).rowValue,
                            @"Other" : ((DLTableRow *)requestedDocumentSectionObj.rowsData[6]).rowValue,
               
                            @"NotifyBorrower" : NotifyBorrower
                            };

    
    [[AFILoanRequestManager manager] GET:@"PrequalMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Save PrequalMobile : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"Prequal Information Successfully Saved."];
            }
            else{
                [AlertManager showAlertSingle:@"Error" msg:[[responseObject objectForKey:@"General"] valueForKey:@"message"]];
                [SVProgressHUD dismiss];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}
-(void)requestToSaveLSU:(NSString*)NotifyBorrower{
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    AKDTableSection *documentationSectionObj = self.tableData[0];
    AKDTableSection *underwritingSectionObj = self.tableData[1];
    AKDTableSection *closingSectionObj = self.tableData[2];
    
    NSDictionary *param = @{
                            @"Update":@"true",
                            @"Loan_Number" :self.loanNo,
                            @"Documentation_1" : ((DLTableRow *)documentationSectionObj.rowsData[0]).boolValue?@"True":@"False",
                            @"Documentation_2" : ((DLTableRow *)documentationSectionObj.rowsData[1]).boolValue?@"True":@"False",
                            @"Documentation_3" :((DLTableRow *)documentationSectionObj.rowsData[2]).boolValue?@"True":@"False",
                            @"Documentation_4" : ((DLTableRow *)documentationSectionObj.rowsData[3]).boolValue?@"True":@"False",
                            @"Documentation_5" :((DLTableRow *)documentationSectionObj.rowsData[4]).boolValue?@"True":@"False",
                            @"Documentation_6" : ((DLTableRow *)documentationSectionObj.rowsData[5]).boolValue?@"True":@"False",
                            @"Documentation_7" : ((DLTableRow *)documentationSectionObj.rowsData[6]).boolValue?@"True":@"False",
                            @"Documentation_8" : ((DLTableRow *)documentationSectionObj.rowsData[7]).boolValue?@"True":@"False",
                            @"Documentation_10" : ((DLTableRow *)documentationSectionObj.rowsData[9]).boolValue?@"True":@"False",
                            @"Documentation_11" :((DLTableRow *)documentationSectionObj.rowsData[10]).boolValue?@"True":@"False",
                            
                            @"Underwriting_1" : ((DLTableRow *)underwritingSectionObj.rowsData[0]).boolValue?@"True":@"False",
                            @"Underwriting_2" : ((DLTableRow *)underwritingSectionObj.rowsData[1]).boolValue?@"True":@"False",
                            @"Underwriting_3" : ((DLTableRow *)underwritingSectionObj.rowsData[2]).boolValue?@"True":@"False",
                            @"Underwriting_4" : ((DLTableRow *)underwritingSectionObj.rowsData[3]).boolValue?@"True":@"False",
                            
                            @"Closing_1" : ((DLTableRow *)closingSectionObj.rowsData[0]).boolValue?@"True":@"False",
                            @"Closing_2" : ((DLTableRow *)closingSectionObj.rowsData[1]).boolValue?@"True":@"False",
                            @"Closing_3" : ((DLTableRow *)closingSectionObj.rowsData[2]).boolValue?@"True":@"False",
                            @"Closing_4" : ((DLTableRow *)closingSectionObj.rowsData[3]).boolValue?@"True":@"False",
                            @"Closing_5" : ((DLTableRow *)closingSectionObj.rowsData[4]).boolValue?@"True":@"False",
                            @"Closing_6" : ((DLTableRow *)closingSectionObj.rowsData[5]).boolValue?@"True":@"False",
                            @"Closing_7" : ((DLTableRow *)closingSectionObj.rowsData[6]).boolValue?@"True":@"False",
                            @"Closing_8" : ((DLTableRow *)closingSectionObj.rowsData[7]).boolValue?@"True":@"False",
                            @"Closing_9" : ((DLTableRow *)closingSectionObj.rowsData[8]).boolValue?@"True":@"False",
                            
                            @"NotifyBorrower" : NotifyBorrower,
                            };
    
    [[AFILoanRequestManager manager] GET:@"LSUMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Update LSUMobile : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            if ([[[responseObject objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                [SVProgressHUD showSuccessWithStatus:@"LSU Information Successfully updated."];
            }
            else{
                [AlertManager showAlertSingle:@"Error" msg:[[responseObject objectForKey:@"General"] valueForKey:@"message"]];
                [SVProgressHUD dismiss];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error getting explore info: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

//------------------------------------------------------
#pragma mark - Button Actions
//------------------------------------------------------

-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:NULL];
}
-(void)saveButtonTapAction:(id)sender
{
    if (self.segmentControl.selectedSegmentIndex==0) {
        //Prequal
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"PreQual Saved." message:@"Would you like to notify the borrower?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        alert.tag=0;
        [alert show];
    }
    else{
        //LSU
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"LSU Saved." message:@"Would you like to notify the borrower?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        alert.tag=1;
        [alert show];
        
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case 0:{
            [self requestToSavePrequal:buttonIndex==0?@"False":@"True"];
            break;
        }
        case 1:{
            [self requestToSaveLSU:buttonIndex==0?@"False":@"True"];
            break;
        }
        case 2:{
            //add agent
            if (buttonIndex==1) {
                if ([InputValidator isEmailValid:[alertView textFieldAtIndex:0].text]) {
                    [self modifyAgentForLoanNumber:self.loanNo
                                       tableViewCell:nil
                                             email:[alertView textFieldAtIndex:0].text
                                              type:@"AddAgent"
                                            status:AgentModificationStatusAdd];
                }
            }
            break;
        }
        default:
            break;
    }
}
-(IBAction)addButtonTapAction:(UIButton*)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add an Agent" message:@"Enter an email address bellow." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].keyboardType=UIKeyboardTypeEmailAddress;
    alert.tag=2;
    [alert show];
}

-(void)collapseButtonTapAction:(id)sender {
    UIButton *button=(UIButton*)sender;
    [self tableView:self.tableView toggleSection:button.tag];
}
-(IBAction)cellPhoneButtonOnAgentTapAction:(UIButton*)sender{
    AgentCell *cell = (AgentCell *)[sender firstSuperviewOfClass:[AgentCell class]];
    if (cell) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        AKDTableSection *sectionObj = self.tableData[indexPath.section];
        DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
        
        NSString *phoneNo;
        phoneNo=[NSString stringWithFormat:@"tel:%@",dLTableRow.rowValue];
        
        if ([phoneNo isEqualToString:@""]) {
            [AlertManager showAlertSingle:@"" msg:@"No phone no available."];
        }
        else{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
        }
    }
}
-(IBAction)msgButtonOnAgentTapAction:(UIButton*)sender{
    AgentCell *cell = (AgentCell *)[sender firstSuperviewOfClass:[AgentCell class]];
    if (cell) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        AKDTableSection *sectionObj = self.tableData[indexPath.section];
        DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
        
        NSString *mailInfo = @"?subject= ";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@%@",dLTableRow.email,mailInfo]]];
        NSLog(@"emailing");
        /*
        NSString *phoneNo;
    
        phoneNo=[NSString stringWithFormat:@"sms:%@",dLTableRow.rowValue];
        
        if ([phoneNo isEqualToString:@""]) {
            [AlertManager showAlertSingle:@"" msg:@"No phone no available."];
        }
        else{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
        }*/
    }
}
-(IBAction)cellPhoneButtonOnContactTapAction:(UIButton*)sender{
    
    LoanContactCell *cell = (LoanContactCell *)[sender firstSuperviewOfClass:[LoanContactCell class]];
    if (cell) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        AKDTableSection *sectionObj = self.tableData[indexPath.section];
        DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
        
        NSString *phoneNo;
        phoneNo=[NSString stringWithFormat:@"tel:%@",dLTableRow.rowValue];
        
        if ([phoneNo isEqualToString:@""]) {
             [AlertManager showAlertSingle:@"" msg:@"No phone no available."];
        }
        else{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
        }
    }
}
-(IBAction)msgButtonOnContactTapAction:(UIButton*)sender{
    LoanContactCell *cell = (LoanContactCell *)[sender firstSuperviewOfClass:[LoanContactCell class]];
    if (cell) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        AKDTableSection *sectionObj = self.tableData[indexPath.section];
        DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
        
        NSString *mailInfo = @"?subject= ";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@%@",dLTableRow.email,mailInfo]]];
        NSLog(@"emailing");
        /*
        NSString *phoneNo;
        
        phoneNo=[NSString stringWithFormat:@"sms:%@",dLTableRow.rowValue];
        
        if ([phoneNo isEqualToString:@""]) {
            [AlertManager showAlertSingle:@"" msg:@"No phone no available."];
        }
        else{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
        }*/
    }
}
#pragma mark - UISwitch Value change Inspector
-(void) switchValueChanged:(UISwitch *)sender{
    if ([sender firstSuperviewOfClass:[GraySwitchCell class]])
    {
        GraySwitchCell *cell = (GraySwitchCell *)[sender firstSuperviewOfClass:[GraySwitchCell class]];
        if (cell) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            AKDTableSection *sectionObj = self.tableData[indexPath.section];
            DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
            dLTableRow.boolValue =sender.on?YES:NO;
        }
    }
}
//------------------------------------------------------
#pragma mark - TableView Delegate/DataSource
//------------------------------------------------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.tableData count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.isCollapsed? 0 : [sectionObj.rowsData count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    AKDTableSection *sectionObj = self.tableData[section];
    return sectionObj.sectionHeaderHeight;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IOS8_OR_ABOVE) {
        return UITableViewAutomaticDimension;
    }
    
    UITableViewCell *cell = nil;
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
    
    switch (dLTableRow.typeID)
    {
        case loanRow:
            cell = self.loanContactCell;
            break;
            
        case commonInputRow:
            cell = self.commonInputCell;
            break;
            
        case switchRow:
            cell = self.commonInputSwitchCell;
            break;
            
        case prequalTriOptionRow:
            cell = self.prequalTriOptionCell;
            break;
        case disabledInputRow:
            cell = self.disabledInputCell;
            break;
            
        case monthlyFeesRow:
            cell = self.monthlyFeesInputCell;
            break;
            
        case commonActionRow:
            cell = self.commonActionCell;
            break;
        case lsuSwitchRow:
            cell = self.graySwitchCell;
            break;
        case prequalSwitchRow:
            cell = self.graySwitchCell;
            break;
            
        default:
            break;
    }
    
    [cell updateConstraintsIfNeeded];
    [cell layoutIfNeeded];
    
    return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    AKDTableSection *sectionObj = self.tableData[section];
    
    AccountHeader *accHeader = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([AccountHeader class])];
    
    OptionalFieldHeader *optionHeader = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([OptionalFieldHeader class])];
    
    switch (sectionObj.typeID)
    {
        case loanHeader:
            
            optionHeader.collapseButton.hidden=YES;
            optionHeader.backView.layer.shadowOffset = CGSizeMake(5, 0);
            optionHeader.titleLabel.text=sectionObj.sectionInfo;
            optionHeader.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            optionHeader.backView.layer.shadowOpacity = 1;
            optionHeader.backView.layer.shadowRadius = 1.0;
            optionHeader.collapseButton.tag=section;
            optionHeader.titleLabel.textColor=[UIColor colorWithRed:60.0/255 green:99.0/255 blue:170.0/255 alpha:1.0];
            headerView = optionHeader;
            
            break;
            
        case monthlyExpensesHeader:
            
            optionHeader.inputTextField.hidden=NO;
            optionHeader.inputTextField.text=[NSString stringWithFormat:@"%@",sectionObj.inputValue];
            optionHeader.backView.layer.shadowOffset = CGSizeMake(5, 0);
            optionHeader.titleLabel.text=sectionObj.sectionInfo;
            optionHeader.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            optionHeader.backView.layer.shadowOpacity = 1;
            optionHeader.backView.layer.shadowRadius = 1.0;
            optionHeader.collapseButton.tag=section;
            [optionHeader.collapseButton addTarget:self action:@selector(collapseButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
            headerView = optionHeader;
            
            break;
            
        case accountHeader:
            
            accHeader.collapseButton.hidden=NO;
            accHeader.collapseButton.tag = section;
            accHeader.titleLabel.text = sectionObj.sectionInfo;
            accHeader.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            accHeader.backView.layer.shadowOpacity = 1;
            accHeader.backView.layer.shadowRadius = 1.0;
            [accHeader.collapseButton addTarget:self action:@selector(collapseButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
            headerView = accHeader;
            
            break;
            
        case agentHeader:
            
            optionHeader.addButton.hidden=NO;
            optionHeader.collapseButton.tag=section;
            optionHeader.addButton.tag=section;
            
            if (![optionHeader.collapseButton.allTargets containsObject:self]) {
                [optionHeader.collapseButton addTarget:self action:@selector(collapseButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            if (![optionHeader.addButton.allTargets containsObject:self]) {
                [optionHeader.addButton addTarget:self action:@selector(addButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            optionHeader.backView.layer.shadowOffset = CGSizeMake(5, 0);
            optionHeader.titleLabel.text=sectionObj.sectionInfo;
            optionHeader.backView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
            optionHeader.backView.layer.shadowOpacity = 1;
            optionHeader.backView.layer.shadowRadius = 1.0;
            headerView = optionHeader;
            
            break;
            
        default:
            break;
    }
    
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
    static NSString *identifier;
    
    if (dLTableRow.typeID==loanRow) {
        identifier = @"LoanContactCell";
        LoanContactCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=[NSString stringWithFormat:@"%@", dLTableRow.rowValue];
        
       // if (sectionObj.typeID==monthlyExpensesHeader) cell.bgView.backgroundColor=[UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1.0];
        cell.cellPhoneButton.tag=indexPath.row;
        cell.msgButton.tag=indexPath.row;
    
        if (![cell.cellPhoneButton.allTargets containsObject:self]) {
            [cell.cellPhoneButton addTarget:self action:@selector(cellPhoneButtonOnContactTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (![cell.msgButton.allTargets containsObject:self]) {
            [cell.msgButton addTarget:self action:@selector(msgButtonOnContactTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        return cell;
    }
    else  if (dLTableRow.typeID==commonInputRow) {
        identifier = @"CommonInputCell";
        CommonInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=[NSString stringWithFormat:@"%@",dLTableRow.rowValue];
         
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        return cell;
    }
    
    else  if (dLTableRow.typeID==monthlyFeesRow) {
        identifier = @"MonthlyFeesInputCell";
        MonthlyFeesInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=[NSString stringWithFormat:@"%@",dLTableRow.rowValue];
        cell.inputField.userInteractionEnabled=dLTableRow.boolValue;
        cell.bgView.backgroundColor=[UIColor colorWithRed:163.0/255 green:163.0/255 blue:163.0/255 alpha:1.0];
        
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        return cell;
    }
    if (dLTableRow.typeID==commonActionRow)
    {
        identifier = @"CommonActionCell";
        CommonActionCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 5);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.actionButton.tag=indexPath.section;
        
        if (![cell.actionButton.allTargets containsObject:self]) {
            [cell.actionButton addTarget:self action:@selector(saveButtonTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell;
    }
    else  if (dLTableRow.typeID==disabledInputRow) {
        identifier = @"DisabledInputCell";
        DisabledInputCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=[NSString stringWithFormat:@"%@",dLTableRow.rowValue];
        cell.bgView.layer.backgroundColor=[[UIColor colorWithRed:218.0/255 green:218.0/255 blue:218.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        return cell;
    }
    else if (dLTableRow.typeID==switchRow) {
        identifier = @"CommonInputSwitchCell";
        CommonInputSwitchCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        //cell.bgView.backgroundColor = [UIColor lightGrayColor];
       // cell.contentView.backgroundColor = [UIColor lightGrayColor];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.syncSwitch.on=dLTableRow.boolValue;
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        return cell;
    }
    else if (dLTableRow.typeID==lsuSwitchRow) {
        identifier = @"GraySwitchCell";
        GraySwitchCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.syncSwitch.on=dLTableRow.boolValue;
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        if (![cell.syncSwitch.allTargets containsObject:self]) {
            [cell.syncSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        }
        return cell;
    }
    else if (dLTableRow.typeID==prequalSwitchRow) {
        identifier = @"GraySwitchCell";
        GraySwitchCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.syncSwitch.on=dLTableRow.boolValue;
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        cell.titleLabel.font = [UIFont systemFontOfSize:14.0];
        if (![cell.syncSwitch.allTargets containsObject:self]) {
            [cell.syncSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        }
        return cell;
    }
    else if (dLTableRow.typeID==prequalTriOptionRow) {
        identifier = @"PrequalTriOptionCell";
        PrequalTriOptionCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.font = [UIFont systemFontOfSize:14.0];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.detailsLabel.text =[NSString stringWithFormat:@"%@",[dLTableRow.rowValue createFormatedPreQualInfo:dLTableRow.rowValue]];
        cell.rightUtilityButtons = [self prequalRightButtons];
        cell.delegate = self;
        return cell;
    }
    else{
        identifier = @"AgentCell";
        AgentCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.titleLabel.text = dLTableRow.rowTitle;
        cell.inputField.text=[NSString stringWithFormat:@"%@", dLTableRow.rowValue];
        
        cell.bgView.layer.shadowColor=[[UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0]CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(5, 0);
        cell.bgView.layer.shadowOpacity = 1;
        cell.bgView.layer.shadowRadius = 1.0;
        
        if (![cell.cellPhoneButton.allTargets containsObject:self]) {
            [cell.cellPhoneButton addTarget:self action:@selector(cellPhoneButtonOnAgentTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (![cell.msgButton.allTargets containsObject:self]) {
            [cell.msgButton addTarget:self action:@selector(msgButtonOnAgentTapAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        //SWTableViewCell Utility Buttons
        cell.leftUtilityButtons = [self leftButtons];
        cell.rightUtilityButtons = [self rightButtons];
        cell.delegate = self;
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

//------------------------------------------------------
#pragma mark - TableView Utils (Private)
//------------------------------------------------------

-(void) tableView:(UITableView *) tableView toggleSection:(NSInteger) section {
    AKDTableSection *sectionObj = self.tableData[section];
    sectionObj.isCollapsed = !sectionObj.isCollapsed;
    
    [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
}

//------------------------------------------------------
#pragma mark - Segment Cotrol TapAction
//------------------------------------------------------

- (IBAction)segmentControlTapAction:(id)sender {
    NSLog(@"segment selected index:%lu",self.segmentControl.selectedSegmentIndex);
    if (self.segmentControl.selectedSegmentIndex==0) {
        //PreQual Section
         [self loadTableViewData:0];
        
    }
    else{
       //LSU Section
         [self loadTableViewData:1];
    }
}

//------------------------------------------------------
#pragma mark - ******** SWTableViewCell *********
#pragma mark - Delegate Methods -
//------------------------------------------------------
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow =sectionObj.rowsData[indexPath.row];
    
    if ([cell isKindOfClass:[PrequalTriOptionCell class]]) {
        switch (index) {
            case 0:
                dLTableRow.rowValue=@"";
                [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                break;
            case 1:
                dLTableRow.rowValue=@"No";
                [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                break;
            case 2:
                dLTableRow.rowValue=@"Yes";
                [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                break;
            default:
                break;
        }
    }
    else{
         [self modifyAgentForLoanNumber:self.loanNo
                        tableViewCell:cell
                                 email:dLTableRow.email
                                  type:@"RemoveAgent"
                                status:AgentModificationStatusRemove];
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    NSLog(@"Left Utility Button Pressed.");
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    DLTableRow *dLTableRow = sectionObj.rowsData[indexPath.row];
    
    [self modifyAgentForLoanNumber:self.loanNo
                       tableViewCell:cell
                             email:dLTableRow.email
                              type:@"Set_Primary_Agent"
                            status:AgentModificationStatusPrimary];
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES; // allow just one cell's utility button to be open at once
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state {
    return YES;
}

//------------------------------------------------------
#pragma mark - Utility Button Methods
//------------------------------------------------------
- (NSArray *)prequalRightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:187/255.0 green:186/255.0 blue:193/255.0 alpha:1.0]
                                                title:@"n/a"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:234/255.0 green:33/255.0 blue:37/255.0 alpha:1.0]
                                                title:@"No"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:35/255.0 green:77/255.0 blue:157/255.0 alpha:1.0]
                                                title:@"Yes"];
    return rightUtilityButtons;
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:227/255.0 green:54/255.0 blue:47/255.0 alpha:1.0]
                                                title:@"Remove"];
    return rightUtilityButtons;
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.0 green:103/255.0 blue:170/255.0 alpha:1.0]
                                                title:@"Primary"];
    return leftUtilityButtons;
}

//------------------------------------------------------
#pragma mark - API
//------------------------------------------------------
-(void) modifyAgentForLoanNumber:(NSString *)loanNumber
                     tableViewCell:(SWTableViewCell *)cell
                           email:(NSString *)email
                            type:(NSString *)type
                          status:(AgentModificationStatus)status
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AKDTableSection *sectionObj = self.tableData[indexPath.section];
    
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [AlertManager showAlertSingle:@"Connection Error!" msg:noInternetAlertMsg];
        return;
    }
    
    NSDictionary *param = @{type : @"true", @"LoanNumber" : loanNumber, @"Email" : email};
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    __weak typeof(self) weakSelf = self;
    
    [[AFILoanRequestManager manager].requestSerializer setAuthorizationHeaderFieldWithUsername:[[AppUserDefault sharedInstance] getStringValueforKey:sessionKey] password:@""];
    
    [[AFILoanRequestManager manager] GET:@"LoanMobile" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Get Encompass Loans : %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]]){
            NSDictionary *result = responseObject;
            if ([[[result objectForKey:@"General"] valueForKey:@"errorCode"] intValue]==0){
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    
                    [SVProgressHUD dismiss];
                    
                    switch (status)
                    {
                        case AgentModificationStatusAdd:
                        {
                            DLTableRow *dLTableRow =[DLTableRow new];
                            dLTableRow.typeID=agentRow;
                            dLTableRow.rowTitle=@"Test Name";
                            dLTableRow.rowValue=@"01725097586";
                            dLTableRow.email=email;
                            AKDTableSection *sectionObj1 = self.tableData[1];
                            [sectionObj1.rowsData addObject:dLTableRow];
                            [strongSelf.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
                            /*
                            NSRange range = NSMakeRange(0, 1);
                            NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
                            [strongSelf.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];*/
                            break;
                        }
                        case AgentModificationStatusRemove:
                            [sectionObj.rowsData removeObjectAtIndex:indexPath.row];
                            [strongSelf.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                            break;
                            
                        case AgentModificationStatusPrimary:
                            [strongSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            break;
                            
                        default:
                            break;
                    }
                }
            }
            else{
                [SVProgressHUD dismiss];
                [AlertManager showAlertSingle:@"" msg:[[result objectForKey:@"General"] valueForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        NSLog(@"Error EncompassLoanInfo: %@ <%@>", error.localizedDescription, operation.request.URL);
    }];
}

@end
