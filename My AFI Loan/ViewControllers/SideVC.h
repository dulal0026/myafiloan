//
//  ViewController.h
//  My AFI Loan
//
//  Created by Dulal Hossain on 4/6/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUserDefault.h"
#import "UIViewController+MMDrawerController.h"
#import "ApplicationTextManager.h"
@interface SideVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end


@interface AAPMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak ,nonatomic) IBOutlet UIView *backView;


@end

