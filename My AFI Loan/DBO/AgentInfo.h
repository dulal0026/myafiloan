//
//  AgentInfo.h
//  MY AFILOAN
//
//  Created by Dulal Hossain on 16/04/2015.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface AgentInfo : JSONModel

@property (nonatomic) BOOL Active;
@property (nonatomic, retain) NSString * Address;
@property (nonatomic, retain) NSString * Company;
@property (nonatomic, retain) NSString * Email;
@property (nonatomic, retain) NSString * Display_Name;
@property (nonatomic, retain) NSString * First_Name;
@property (nonatomic, retain) NSString * Last_Name;
@property (nonatomic, retain) NSString * MobilePhone;
@property (nonatomic, retain) NSString * OfficePhone;
@property (nonatomic, retain) NSString * Photo_URL;

@end
