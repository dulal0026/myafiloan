//
//  AgentInfo.h
//  MY AFILOAN
//
//  Created by Dulal Hossain on 16/04/2015.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface LoanContactInfoBBA : JSONModel

@property (nonatomic, retain) NSString * Name;
@property (nonatomic, retain) NSString * Email;
@property (nonatomic, retain) NSString * PhoneNumber;
@property (nonatomic) NSInteger usertype;
@end
