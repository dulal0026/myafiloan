//
//  AgentInfo.h
//  MY AFILOAN
//
//  Created by Dulal Hossain on 16/04/2015.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface LoanInfoBBA : JSONModel

@property (nonatomic) double PurchasePrice;
@property (nonatomic) double LoanAmount;
@property (nonatomic) double DownPayment;
@property (nonatomic) double InterestRate;
@property (nonatomic) double LoanTerm;

@property (nonatomic) double MonthlyFees;
@property (nonatomic) double MonthlyPayment;
@property (nonatomic) double OtherFinancingPAndI;
@property (nonatomic) double HazardInsurance;
@property (nonatomic) double RealEstateTaxes;

@property (nonatomic) double MortgageInsurance;
@property (nonatomic) double HOADues;
@property (nonatomic) double OtherExpenses;

@property (nonatomic) BOOL LSUPDFExist;
@property (nonatomic) BOOL PrequalPDFExist;

@end
