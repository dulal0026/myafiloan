//
//  BusinessInfo.h
//  MY AFILOAN
//
//  Created by AAPBD Mac mini on 26/01/2015.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface LoanInfo : JSONModel
@property (nonatomic, retain) NSString * Loan_Number;
@property (nonatomic, retain) NSString * Borrower_First_Name;
@property (nonatomic, retain) NSString * Borrower_Last_Name;
@property (nonatomic) BOOL Archived;

@end
