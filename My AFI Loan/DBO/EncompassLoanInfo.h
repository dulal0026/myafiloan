//
//  EncompassLoanInfo.h
//  MY AFILOAN
//
//  Created by Dulal Hossain mini on 01/05/2015.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface EncompassLoanInfo : JSONModel
@property (nonatomic, retain) NSString * LoanNumber;
@property (nonatomic, retain) NSString * BorrowerLastName;
@end
